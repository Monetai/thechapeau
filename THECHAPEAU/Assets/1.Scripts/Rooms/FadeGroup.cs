﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    [System.Serializable]
    public struct FadeGroupParameters
    {
        [BoxGroup("Fade Parameters")]
        public float fadeTargetValue;
        [BoxGroup("Fade Parameters")]
        public float fadeDistanceForTarget;
        [BoxGroup("Fade Parameters")]
        public float fadeDistanceForBegin;
    }

    public class FadeGroup : MonoBehaviour
    {
        public FadeGroupParameters parameters;
        public List<GameObject> objectsToFade;
        private List<Renderer> renderersToFade;
        private float groupAlpha = 1f;

        // Use this for initialization
        void Start()
        {
            renderersToFade = new List<Renderer>();
            Renderer[] renderers;
            foreach (GameObject obj in objectsToFade)
            {
                renderers = obj.GetComponentsInChildren<Renderer>();
                renderersToFade.AddRange(renderers);
            }
        }

        private void Update()
        {
			if (Mathf.Abs (Camera.main.transform.position.z - transform.position.z) < parameters.fadeDistanceForBegin) {
				float min = transform.position.z - parameters.fadeDistanceForTarget;
				float max = transform.position.z - parameters.fadeDistanceForBegin;

				float normalizedDistance = -(Camera.main.transform.position.z - max) / (max - min);

				groupAlpha = Mathf.Lerp (1, parameters.fadeTargetValue, normalizedDistance);
				SetAlphaTo (groupAlpha);
			} else if (groupAlpha != 0 || groupAlpha != 1) {
				groupAlpha = Mathf.Round (groupAlpha);
				SetAlphaTo (groupAlpha);
			}

            if (Input.GetKeyDown(KeyCode.A))
            {
                foreach (Renderer rend in renderersToFade)
                {
                    StandardShaderUtils.ChangeRenderMode(rend.material, StandardShaderUtils.BlendMode.Fade);
                }
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                foreach (Renderer rend in renderersToFade)
                {
                    StandardShaderUtils.ChangeRenderMode(rend.material, StandardShaderUtils.BlendMode.Opaque);
                }
            }
        }

        private void SetAlphaTo(float target)
        {
			target = Mathf.Clamp01 (target);
            Color col;
            foreach (Renderer rend in renderersToFade)
            {
                StandardShaderUtils.ChangeRenderMode(rend.material, StandardShaderUtils.BlendMode.Fade);
                rend.enabled = true;
                col = rend.material.color;
                col.a = target;
                rend.material.color = col;
            }

            if (target < 1)
            {
                foreach (Renderer rend in renderersToFade)
                {
                    if (target <= 0)
                        rend.enabled = false;
                    StandardShaderUtils.ChangeRenderMode(rend.material, StandardShaderUtils.BlendMode.Fade);
                }
            }
            else if (target == 1)
            {
                foreach (Renderer rend in renderersToFade)
                {
                    rend.enabled = true;
                    StandardShaderUtils.ChangeRenderMode(rend.material, StandardShaderUtils.BlendMode.Opaque);
                }
            }
        }
    }
}