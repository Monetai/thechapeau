﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace LittleWitch
{
    public class Door : MonoBehaviour
    {
        private Room roomFront;
        private Room roomBehind;

        private void Awake()
        {
            roomFront = Room.SearchForRoom(transform.position + transform.forward * 2);
            roomBehind = Room.SearchForRoom(transform.position + (-transform.forward) * 2);
        }

        void Start()
        {
            if (roomFront)
                roomFront.AddDoor(this);
            if (roomBehind)
                roomBehind.AddDoor(this);
        }

        public Room GetActiveRoom()
        {
            if (roomFront && roomFront.isActiveAndEnabled)
                return roomFront;
            else if (roomBehind && roomBehind.isActiveAndEnabled)
                return roomBehind;
            else return null;
        }

        public Room GetInactiveRoom()
        {
            if (roomFront && !roomFront.isActiveAndEnabled)
                return roomFront;
            else if (roomBehind && !roomBehind.isActiveAndEnabled)
                return roomBehind;
            else return null;
        }

        public void NotifyRooms()
        {
            if (roomFront && roomFront.ContainsPlayer() == false)
                roomFront.SetRoomState(false);

            if (roomBehind && roomBehind.ContainsPlayer() == false)
                roomBehind.SetRoomState(false);
        }

        public void ActivateBothRoom()
        {
            roomFront.SetRoomState(true);
            roomBehind.SetRoomState(true);
        }

        public bool CanPassThrough()
        {
            if (roomFront && roomBehind)
                return true;
            else return false;
        }

        /// <summary>
        /// Return 1 if the target is in front, -1 if behind
        /// </summary>
        /// <param name="lookTarget"></param>
        /// <returns></returns>
        public int GetSideFromDoor(Transform lookTarget)
        {
            Vector3 dir = (lookTarget.transform.position - transform.position).normalized;
            float direction = Vector3.Dot(dir, transform.forward);
            if (direction < 0)
                return -1;
            else
                return 1;
        }

        private void OnDrawGizmosSelected()
        {
            Room A = Room.SearchForRoom(transform.position + transform.forward * 2);
            Room B = Room.SearchForRoom(transform.position + (-transform.forward) * 2);

            if (A)
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;

#if UNITY_EDITOR
            Handles.color = Color.black;
            Handles.Label(transform.position + transform.forward * 2 + Vector3.up, "Front");
#endif
            Gizmos.DrawSphere(transform.position + transform.forward * 2, 0.5f);

            if (B)
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;

#if UNITY_EDITOR
            Handles.color = Color.black;
            Handles.Label(transform.position + (-transform.forward) * 2 + Vector3.up , "Back");
#endif
            Gizmos.DrawSphere(transform.position + (-transform.forward) * 2, 0.5f);

            Gizmos.color = Color.blue;
        }
    }
}