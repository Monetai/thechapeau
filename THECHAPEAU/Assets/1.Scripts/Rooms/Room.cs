﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class Room : MonoBehaviour
    {
        Transform player;
        BaseInteraction[] roomInteractions;
        List<Door> roomDoors = new List<Door>();
        Collider[] cols;
        Bounds roomBounds;

        void Awake()
        {
            if(cols == null)
                cols = GetComponents<Collider>();

            roomBounds = GetRoomBounds();
        }

        private void Start()
        {
            player = GameplayManager.Instance.currentPlayer.transform;
            SetRoomState(ContainsPlayer());
        }
    
        public bool ContainsPlayer()
        {
            if(player == null)
                player = GameplayManager.Instance.currentPlayer.transform;

            if(cols == null)
                cols = GetComponents<Collider>();

            bool containPlayer = false;

            if (roomBounds.Contains(player.position))
                containPlayer = true;

            return containPlayer;
        }

        Bounds GetRoomBounds()
        {
            var bounds = new Bounds(transform.position, Vector3.zero);
            foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            {
                bounds.Encapsulate(rend.bounds);
            }
            return bounds;
        }

        /// <summary>
        /// Set the room's state, true if active, false if inactive
        /// </summary>
        /// <param name="state"></param>
        public void SetRoomState(bool state)
        {
            gameObject.SetActive(state);
        }

        public BaseInteraction[] GetAllInteractionInRoom()
        {
            if(roomInteractions == null)
            {
                List<BaseInteraction> interacts = new List<BaseInteraction>(GetComponentsInChildren<BaseInteraction>());
                foreach(Door door in roomDoors)
                {
                    BaseInteraction doorInteract = door.GetComponent<BaseInteraction>();
                    if (doorInteract)
                        interacts.Add(doorInteract);
                }
                roomInteractions = interacts.ToArray();
            }
            return roomInteractions;
        }

        public void AddDoor(Door door)
        {
            roomDoors.Add(door);
        }

        /// <summary>
        /// Return if a room is in the direction;
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public static Room SearchForRoom(Vector3 position, float size = 1)
        {
            Collider[] colRoom = Physics.OverlapSphere(position, size);
            return colRoom.Length > 0 ? colRoom[0].GetComponentInParent<Room>() : null;
        }
    }
}