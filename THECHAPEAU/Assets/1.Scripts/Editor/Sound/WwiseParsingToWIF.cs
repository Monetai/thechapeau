﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;

namespace LittleWitch
{
    public class WwiseParsingToWIF
    {
        //------------------------------------------------------------------------------
        // values to change on an other project with the wifsettings scriptable
        static string fileWwiseIds = "";
        static string folderForWIF = "";
        static string folderForConstManager = "";
        static string folderForSettings = "Assets";

        //------------------------------------------------------------------------------
        // init files and folder infos        
        string debugStringList = "";                        // debug message
        StreamReader fileToRead;                            // open the .h file to read 
        string line = "";                                   // line for each line of the file
        DirectoryInfo info;                                 // infos about directory
        FileInfo[] fileInfo;                                // infos about file
        TextWriter textWriter;                              // text writer for creation file
        private static bool prefsLoaded = false;            // for the UI

        //------------------------------------------------------------------------------
        // init WIF infos
        bool fileExists;                                       // check if the file exist
        List<string> wwiseIDsList = new List<string>();        // list of current elements
        List<WIFEvents> wIFEventAdded = new List<WIFEvents>(); // list of WIF Event Added
        List<string> wIFEventRemoved = new List<string>();     // list of WIF Event Deleted
        List<string[]> wIFEventsList = new List<string[]>();   // list of WIF All Event
        List<string[]> wIFBankList = new List<string[]>();     // list of banks
        List<string[]> wIFBussesList = new List<string[]>();   // list of busses
        List<string[]> wIFRTPCList = new List<string[]>();     // list of game parameters
        List<string[]> wIFTriggerList = new List<string[]>();  // list of triggers
        List<List<string[]>> wIFSwitchesList = new List<List<string[]>>();   // list of switches
        List<List<string[]>> wIFStatesList = new List<List<string[]>>();   // list of switches

        //------------------------------------------------------------------------------
        // parse the file and create files
        public void ParseWwiseIDsFile()
        {
            debugStringList += "Wwise Parsing Done\n";
            debugStringList += "(click for more infos...)\n";
            debugStringList += "\n";

            // clean all lists
            wwiseIDsList.Clear();
            wIFEventAdded.Clear();
            wIFEventRemoved.Clear();

            // Init wifSettings
            Object[] wIFSettings = Resources.LoadAll("", typeof(WwiseImporterSettings));
            WwiseImporterSettings wIFSettingsRef = null;

            if (wIFSettings.Length >= 1)
            {
                wIFSettingsRef = wIFSettings[0] as WwiseImporterSettings;
            }

            // create asset if doesn't exist
            if (wIFSettingsRef == null)
            {
                // create folder for the scriptable
                FolderCreation(folderForSettings);

                // create assets
                wIFSettingsRef = ScriptableObject.CreateInstance<WwiseImporterSettings>();
                AssetDatabase.CreateAsset(wIFSettingsRef, folderForSettings + "/WwiseImporterSettings.asset");
                AssetDatabase.SaveAssets();

                debugStringList += "- File : \"" + folderForSettings + "/WwiseImporterSettings.asset\" Created \n";
            }

            // init with the settings values
            fileWwiseIds = wIFSettingsRef.fileWwiseIds;
            folderForWIF = wIFSettingsRef.dirWIFEvents;
            folderForConstManager = wIFSettingsRef.dirWwiseConsts;
            folderForSettings = wIFSettingsRef.dirWwiseImporterSettings;

            // create good folder if doesn't exists
            FolderCreation(folderForWIF);

            // parse the event files
            ParseForNamespaceEvents();

            // remove unused wif events
            RemoveUnusedWIFEvents();

            // parse other elements in files
            ParseForNamespaceOthers("BANKS", wIFBankList);
            ParseForNamespaceOthers("BUSSES", wIFBussesList);
            ParseForNamespaceOthers("GAME_PARAMETERS", wIFRTPCList);
            ParseForNamespaceOthers("TRIGGERS", wIFTriggerList);

            // parse switch and states
            ParseForNamespaceComposite("SWITCHES", wIFSwitchesList);
            ParseForNamespaceComposite("STATES", wIFStatesList);

            // display the pop up
            WIFPopUp.ShowWIFEventMajPopUp(wIFEventAdded, wIFEventRemoved);

            // update the wif manager file
            UpdateWIFManager();

            // show debug list
            Debug.Log(debugStringList);
        }


        //--------------------------------------------------------------------------
        // folder creation with debug message
        public void UpdateWIFManager()
        {
            TextWriter textWriter = new StreamWriter(folderForConstManager + "/WwiseConsts.cs");

            textWriter.WriteLine("namespace LittleWitch {\n");
            {
                textWriter.WriteLine("\tpublic static class WwiseConstsInfos {");
                {
                    textWriter.WriteLine("\t\tpublic const string pathToWIFEvents = \"" + folderForWIF + "\";");
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WIFConstsEvents {");
                {
                    foreach (string[] wIFEvent in wIFEventsList)
                    {
                        textWriter.WriteLine("\t\tpublic const string " + wIFEvent[0] + " = \"" + wIFEvent[0] + "\";");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsBanks {");
                {
                    foreach (string[] wIFBank in wIFBankList)
                    {
                        textWriter.WriteLine("\t\tpublic const string " + wIFBank[0] + " = \"" + wIFBank[0] + "\";");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsBusses {");
                {
                    foreach (string[] wIFBusses in wIFBussesList)
                    {
                        textWriter.WriteLine("\t\tpublic const string " + wIFBusses[0] + " = \"" + wIFBusses[0] + "\";");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsRTPC {");
                {
                    foreach (string[] wIFRTPC in wIFRTPCList)
                    {
                        textWriter.WriteLine("\t\tpublic const string " + wIFRTPC[0] + " = \"" + wIFRTPC[0] + "\";");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsTriggers {");
                {
                    foreach (string[] wIFTrigger in wIFTriggerList)
                    {
                        textWriter.WriteLine("\t\tpublic const string " + wIFTrigger[0] + " = \"" + wIFTrigger[0] + "\";");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsSwitches {");
                {
                    foreach (List<string[]> list in wIFSwitchesList)
                    {
                        bool titleWritted = false;
                        foreach (string[] wIFSwitch in list)
                        {
                            if (!titleWritted)
                            {
                                textWriter.WriteLine("\t\tpublic struct " + wIFSwitch[0] + " {");
                                textWriter.WriteLine("\t\t\tpublic const string SWITCHGROUPNAME = \"" + wIFSwitch[0] + "\";");
                                textWriter.WriteLine("\t\t\tpublic const uint SWITCHGROUPID = " + wIFSwitch[1] + ";");

                                titleWritted = true;
                            }
                            else
                            {
                                textWriter.WriteLine("\t\t\tpublic const uint " + wIFSwitch[0] + " = " + wIFSwitch[1] + ";");
                            }
                        }
                        textWriter.WriteLine("\t\t}");
                    }
                }
                textWriter.WriteLine("\t}\n");


                textWriter.WriteLine("\tpublic static class WwiseConstsStates {");
                {
                    foreach (List<string[]> list in wIFStatesList)
                    {
                        bool titleWritted = false;
                        foreach (string[] wIFState in list)
                        {
                            if (!titleWritted)
                            {
                                textWriter.WriteLine("\t\tpublic struct " + wIFState[0] + " {");
                                textWriter.WriteLine("\t\t\tpublic const string STATEGROUPNAME = \"" + wIFState[0] + "\";");
                                textWriter.WriteLine("\t\t\tpublic const uint STATEGROUPID = " + wIFState[1] + ";");

                                titleWritted = true;
                            }
                            else
                            {
                                textWriter.WriteLine("\t\t\tpublic const uint " + wIFState[0] + " = " + wIFState[1] + ";");
                            }
                        }
                        textWriter.WriteLine("\t\t}");
                    }

                }
                textWriter.WriteLine("\t}\n");
            }
            textWriter.WriteLine("}");

            textWriter.Close();


            debugStringList += "- WIFConsts.cs Update\n";
        }


        //--------------------------------------------------------------------------
        // folder creation with debug message
        public void FolderCreation(string folderName)
        {
            // create folder to use if not exist
            if (!Directory.Exists(folderName))
            {
                debugStringList += "- Folder : \"" + folderName + "\" Created\n";
                Directory.CreateDirectory(folderName);
            }
        }


        //--------------------------------------------------------------------------
        // read the .h file to parse and import switch and states
        public void ParseForNamespaceComposite(string wwiseNamespace, List<List<string[]>> lists)
        {
            // open the .h file
            fileToRead = new StreamReader(fileWwiseIds);
            while ((line = fileToRead.ReadLine()) != null)
            {
                if (line.Contains("namespace " + wwiseNamespace))
                {
                    // jump two lines
                    line = fileToRead.ReadLine();
                    line = fileToRead.ReadLine();

                    // all events
                    while (!line.Contains("}"))
                    {
                        // regular expression to keep the name of switch group
                        string switchGroupName;
                        string switchGroupID;
                        List<string[]> list = new List<string[]>();

                        line = Regex.Replace(line, "        namespace ", "");
                        line = Regex.Replace(line, "", "");
                        switchGroupName = line;

                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();

                        line = Regex.Replace(line, "            static const AkUniqueID GROUP = ", "");
                        line = Regex.Replace(line, "U;", "");
                        switchGroupID = line;

                        // add to the list the name of switch group
                        list.Add(new string[] { switchGroupName, switchGroupID });

                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();

                        while (!line.Contains("}"))
                        {
                            // regular expression to keep necessary content
                            line = Regex.Replace(line, "        static const AkUniqueID ", "");
                            line = Regex.Replace(line, " ", "");
                            line = Regex.Replace(line, "U;", "");

                            // keep name + ID
                            string[] nameAndID = Regex.Split(line, "=");

                            // add switch in list
                            list.Add(new string[] { nameAndID[0], nameAndID[1] });

                            line = fileToRead.ReadLine();
                        }

                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();
                        line = fileToRead.ReadLine();

                        // add the list switch in the global list
                        lists.Add(list);
                    }
                    // next line to exit the container
                    line = fileToRead.ReadLine();
                    line = fileToRead.ReadLine();
                }
            }
            // close the .h file
            fileToRead.Close();
        }


        //--------------------------------------------------------------------------
        // read the .h file to parse and import others
        public void ParseForNamespaceOthers(string wwiseNamespace, List<string[]> list)
        {
            // open the .h file
            fileToRead = new StreamReader(fileWwiseIds);
            while ((line = fileToRead.ReadLine()) != null)
            {
                if (line.Contains("namespace " + wwiseNamespace))
                {
                    // jump two lines
                    line = fileToRead.ReadLine();
                    line = fileToRead.ReadLine();

                    // all events
                    while (!line.Contains("}"))
                    {
                        // regular expression to keep necessary content
                        line = Regex.Replace(line, "        static const AkUniqueID ", "");
                        line = Regex.Replace(line, " ", "");
                        line = Regex.Replace(line, "U;", "");

                        // keep name + ID
                        string[] nameAndID = Regex.Split(line, "=");

                        // keep names in list
                        list.Add(nameAndID);

                        // next line to exit the container
                        line = fileToRead.ReadLine();
                    }
                }
            }
            // close the .h file
            fileToRead.Close();
        }


        //--------------------------------------------------------------------------
        // read the .h file to parse and import events
        public void ParseForNamespaceEvents()
        {
            // open the .h file
            fileToRead = new StreamReader(fileWwiseIds);
            while ((line = fileToRead.ReadLine()) != null)
            {
                if (line.Contains("namespace EVENTS"))
                {
                    // jump two lines
                    line = fileToRead.ReadLine();
                    line = fileToRead.ReadLine();

                    // all events
                    while (!line.Contains("}"))
                    {
                        // regular expression to keep necessary content
                        line = Regex.Replace(line, "        static const AkUniqueID ", "");
                        line = Regex.Replace(line, " ", "");
                        line = Regex.Replace(line, "U;", "");

                        // keep name + ID
                        string[] nameAndID = Regex.Split(line, "=");

                        // keep names in list
                        wwiseIDsList.Add(nameAndID[0]);
                        wIFEventsList.Add(nameAndID);

                        // added wif events assets list
                        AddedNewWIFEvents(folderForWIF + "/", nameAndID[0], nameAndID[1]);

                        // next line to exit the container
                        line = fileToRead.ReadLine();
                    }
                }
            }
            // close the .h file
            fileToRead.Close();
        }

        //--------------------------------------------------------------------------
        // added new wif events
        public void AddedNewWIFEvents(string folder, string wIFName, string wIFID)
        {
            // instantiate WIFEvents 
            info = new DirectoryInfo(folderForWIF);
            fileInfo = info.GetFiles();
            bool fileExists = false;

            foreach (FileInfo file in fileInfo)
            {
                if (file.Name == wIFName + ".asset")
                {
                    fileExists = true;
                }
            }

            // create assets if doesn't exists
            if (!fileExists)
            {
                // instantiate WIFEvents
                WIFEvents wIFEvent = ScriptableObject.CreateInstance<WIFEvents>();
                wIFEvent.eventName = wIFName;
                wIFEvent.eventID = wIFID;
                // create assets
                AssetDatabase.CreateAsset(wIFEvent, folderForWIF + "/" + wIFEvent.eventName + ".asset");
                AssetDatabase.SaveAssets();
                // keep the event added for the pop up
                wIFEventAdded.Add(wIFEvent);
            }
        }

        //--------------------------------------------------------------------------
        // remove unused wif events
        public void RemoveUnusedWIFEvents()
        {
            // remove unused assets in events
            info = new DirectoryInfo(folderForWIF);
            fileInfo = info.GetFiles();
            bool fileExists = false;

            foreach (FileInfo file in fileInfo)
            {
                fileExists = false;

                foreach (string wIFEventCreated in wwiseIDsList)
                {
                    if (file.Name == wIFEventCreated + ".asset" || file.Name == wIFEventCreated + ".asset.meta")
                    {
                        fileExists = true;
                    }
                }

                if (fileExists == false)
                {
                    AssetDatabase.MoveAssetToTrash(folderForWIF + "/" + file.Name);
                    // keep the event deleted for the pop up
                    if (!file.Name.Contains(".meta"))
                        wIFEventRemoved.Add(file.Name);
                }
            }
        }



        //--------------------------------------------------------------------------
        // display menu for import
        [MenuItem("LittleWitch/Sound/BuildWIF %&w")]
        public static void BuildWIF()
        {
            if (!prefsLoaded)
            {
                folderForWIF = EditorPrefs.GetString("folderForWIF", folderForWIF);
                fileWwiseIds = EditorPrefs.GetString("fileWwiseIDs", fileWwiseIds);
                folderForConstManager = EditorPrefs.GetString("folderForManager", folderForConstManager);
                prefsLoaded = true;
            }

            WwiseParsingToWIF wwiseParsingToWIF = new WwiseParsingToWIF();

            wwiseParsingToWIF.ParseWwiseIDsFile();
        }
    }
}