﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public static class RoomsEvents
    {
        public static string OnDoorOpened = "OnDoorOpened";
        public static string OnDoorClosed = "OnDoorClosed";
    }
}
