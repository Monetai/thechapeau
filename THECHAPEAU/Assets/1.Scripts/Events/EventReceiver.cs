﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using com.ootii.Messages;

using Sirenix.OdinInspector;
namespace LittleWitch
{
    public class EventReceiver : MonoBehaviour {

        public EventAsset eventToListen;
        public UnityEvent onReceiveEvent;

        // Use this for initialization
        void OnEnable () {
            if(eventToListen != null)
                MessageDispatcher.AddListener(eventToListen.eventName, OnEventReceived_Internal);
	    }

        private void OnDisable()
        {
            if (eventToListen != null)
                MessageDispatcher.RemoveListener(eventToListen.eventName, OnEventReceived_Internal);
        }

        // Update is called once per frame
        void OnEventReceived_Internal (IMessage mess)
        {
            if (enabled)
                onReceiveEvent.Invoke();
        }

        public void InvokeEvent()
        {
            OnEventReceived_Internal(null);
        }
    }
}
