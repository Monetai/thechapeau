﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using com.ootii.Messages;
namespace LittleWitch
{
    public class AnimationEventSender : MonoBehaviour
    {
        public void OnAnimationEvent(EventAsset eventToSend)
        {
            if (eventToSend != null)
            {
                MessageDispatcher.SendMessage(eventToSend.eventName);
            }
        }
    }
}