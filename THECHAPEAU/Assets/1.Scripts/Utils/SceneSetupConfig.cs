using UnityEngine;
using System.Collections.Generic;

namespace LittleWitch
{
    public sealed class SceneSetupConfig : ScriptableObject
    {
        public List<GameObject> prefabsToInstantiate;

        public void InstantiatePrefabs()
        {
            foreach (GameObject obj in prefabsToInstantiate)
            {
                Instantiate(obj);
            }
        }
    }
}