﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

namespace LittleWitch
{
    public class GameplayManager : MonoBehaviour  {

        public static GameplayManager Instance;
        public Room currentRoom;
        public GameObject currentPlayer ;

        Room[] levelrooms;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            Instance = this;
            currentPlayer = GameObject.FindGameObjectWithTag("Player");

            MessageDispatcher.AddListener(RoomsEvents.OnDoorClosed, OnRoomChanged);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            currentPlayer = GameObject.FindGameObjectWithTag("Player");
            Start();
        }

        private void Start()
        {
            levelrooms = FindObjectsOfType<Room>();
            currentRoom = FindCurrentRoom();
        }

        Room FindCurrentRoom()
        {
            Room current = null;
            for (int i = 0; i < levelrooms.Length; i++)
            {
                if (levelrooms[i].ContainsPlayer())
                {
                    current = levelrooms[i];
                    break;
                }
            }
            return current;
        }

        void OnRoomChanged(IMessage mess)
        {
            currentRoom = FindCurrentRoom();
        }

        private void OnDestroy()
        {
            MessageDispatcher.RemoveListener(RoomsEvents.OnDoorClosed, OnRoomChanged);
        }
    }
}