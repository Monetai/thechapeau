﻿using UnityEngine;
using System.Collections;

namespace LittleWitch
{
    /// <summary>
    /// Class to handle switching from different possible input source
    /// </summary>
    public class InputSourceManager : MonoBehaviour
    {
        InputSource currentSource;
        InputSource prevSource;

        // Use this for initialization
        void Awake()
        {
            currentSource = GetComponent<InputSource>();

            if (!currentSource)
            {
                currentSource = gameObject.AddComponent<NoInputSource>();
            }
        }

        public void SetNoInputs()
        {
            prevSource = currentSource;
            currentSource = gameObject.AddComponent<NoInputSource>();
        }

        public void SetToPrevInput()
        {
            InputSource trans = currentSource;
            currentSource = prevSource;
            prevSource = trans;
        }

        public void SetInputSource(InputSource source)
        {
            prevSource = currentSource;
            currentSource = source;
        }

        public InputSource SetInputSource()
        {
            return currentSource;
        }

        public InputSource GetCurrentInputSource()
        {
            return currentSource;
        }

        public InputSource GetPreviousInputSource()
        {
            return prevSource;
        }
    }
}