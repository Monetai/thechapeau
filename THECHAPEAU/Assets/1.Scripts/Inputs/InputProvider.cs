﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace LittleWitch
{
    public class InputProvider : MonoBehaviour
    {
        Player rePlayer;
        public static InputProvider Instance;

        [System.NonSerialized] // Don't serialize this so the value is lost on an editor script recompile.
        private bool initialized;

        // Use this for initialization
        public void Awake()
        {
            DontDestroyOnLoad(this);
            Instance = this;
            Initialize();
        }

        private void Initialize()
        {
            rePlayer = ReInput.players.GetPlayer(0);
            initialized = true;
        }

        public bool GetAction(int action)
        {
            if (rePlayer != null)
                return rePlayer.GetButton(action);
            else
                return false;
        }

        public bool GetActionDown(int action)
        {
            if (rePlayer != null)
                return rePlayer.GetButtonDown(action);
            else
                return false;
        }

        public bool GetActionUp(int action)
        {
            if (rePlayer != null)
                return rePlayer.GetButtonUp(action);
            else
                return false;
        }

        public float GetAxis(int axis)
        {
            if (rePlayer != null)
                return rePlayer.GetAxis(axis);
            else
                return 0f;
        }

        public bool GetButtonRepeating(int action)
        {
            if (rePlayer != null)
                return rePlayer.GetButtonRepeating(action);
            else
                return false;
        }

        public void SetMapState(bool state, int category)
        {
            rePlayer.controllers.maps.SetMapsEnabled(state, category);

        }

        private void Start()
        {
        }

        private void Update()
        {
            if (!ReInput.isReady) return; // Exit if Rewired isn't ready. This would only happen during a script recompile in the editor.
            if (!initialized) Initialize(); // Reinitialize after a recompile in the editor
        }
    }
}
