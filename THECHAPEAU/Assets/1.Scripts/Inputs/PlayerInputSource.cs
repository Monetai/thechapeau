﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace LittleWitch
{
    public class PlayerInputSource :  InputSource
    {
        //Left Stick
        public override float GetMoveHorizontal()
        {
            return InputProvider.Instance.GetAxis(RewiredConsts.Action.Default.MoveHorizontal);
        }

        public override float GetMoveVertical()
        {
            return InputProvider.Instance.GetAxis(RewiredConsts.Action.Default.MoveVertical);
        }

        //Run Button
        public override bool GetRunButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.Run);
        }

        public override bool GetRunButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.Run);
        }

        public override bool GetRunButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.Run);
        }

        //Interact Button
        public override bool GetInteractButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.Interact);
        }

        public override bool GetInteractButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.Interact);
        }

        public override bool GetInteractButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.Interact);
        }

        //Fire Button
        public override bool GetFireButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.Fire);
        }

        public override bool GetFireButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.Fire);
        }

        public override bool GetFireButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.Fire);
        }

        //Open Menu Tools
        public override bool GetOpenMenuToolsButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.OpenMenuTools);
        }

        public override bool GetOpenMenuToolsButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.OpenMenuTools);
        }

        public override bool GetOpenMenuToolsButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.OpenMenuTools);
        }
    }
}
