﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class ProtoCamera : MonoBehaviour
    {

        public Transform point1;
        public Transform point2;
        public Transform movingPoint;

        public Vector3 ProjectOnLine(Vector3 pointA, Vector3 pointB, Vector3 pointToProject)
        {
            var line = (pointB - pointA);
            var len = line.magnitude;
            line.Normalize();

            var v = pointToProject - pointA;
            var d = Vector3.Dot(v, line);
            d = Mathf.Clamp(d, 0f, len);
            return pointA + line * d;
        }

        private void Update()
        {
            transform.position = ProjectOnLine(point1.position, point2.position, movingPoint.position);
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(point1.position, 0.2f);
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(point2.position, 0.2f);

            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(movingPoint.position, 0.2f);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(ProjectOnLine(point1.position, point2.position, movingPoint.position), 0.2f);

            Gizmos.DrawRay(point2.position, point1.position - point2.position);
        }
    }
}