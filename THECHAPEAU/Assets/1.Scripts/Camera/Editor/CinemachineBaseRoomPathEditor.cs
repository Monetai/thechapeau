﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using UnityEditorInternal;
using Cinemachine.Utility;

namespace Cinemachine.Editor
{
    [CustomEditor(typeof(CinemachineBaseRoomPath))]
    internal sealed class CinemachineBaseRoomPathEditor :  BaseEditor<CinemachinePath>
    {
        private new CinemachineBaseRoomPath Target { get { return target as CinemachineBaseRoomPath; } }
        private static string[] m_excludeFields = null;

        void OnEnable()
        {
            InitWaypointList();
        }

        public override void OnInspectorGUI()
        {
            UpdateWaypointList();

            // Ordinary properties
            if (m_excludeFields == null)
            {
                m_excludeFields = new string[]
                {
                    "m_Script",
                    SerializedPropertyHelper.PropertyName(() => Target.m_Waypoints)
                };
            }
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, m_excludeFields);
            serializedObject.ApplyModifiedProperties();

        }

        void InitWaypointList()
        {
            Target.m_Waypoints = new CinemachinePath.Waypoint[2];
            Target.m_Waypoints[0] = new CinemachinePath.Waypoint();
            Target.m_Waypoints[1] = new CinemachinePath.Waypoint();
        }

        void UpdateWaypointList()
        {
            Target.m_Waypoints[0].position = Target.beginPath;
            Target.m_Waypoints[1].position = Target.endPath;
        }

        void OnSceneGUI()
        {
            //UpdateWaypointList();

            if (Tools.current == Tool.Move)
            {
                Matrix4x4 mOld = Handles.matrix;
                Color colorOld = Handles.color;

                Handles.matrix = Target.transform.localToWorldMatrix;
                for (int i = 0; i < Target.m_Waypoints.Length; ++i)
                {
                    //DrawSelectionHandle(i);
                    DrawPositionControl(i);

                }
                Handles.color = colorOld;
                Handles.matrix = mOld;
            }
        }

        void DrawSelectionHandle(int i)
        {
            if (Event.current.button != 1)
            {
                Vector3 pos = Target.m_Waypoints[i].position;
                //float size = HandleUtility.GetHandleSize(pos) * 0.2f;
                Handles.color = Color.white;
                //if (Handles.Button(pos, Quaternion.identity, size, size, Handles.SphereHandleCap)
                //    && mWaypointList.index != i)
                //{
                //    mWaypointList.index = i;
                //    InternalEditorUtility.RepaintAllViews();
                //}
                // Label it
                Handles.BeginGUI();
                Vector2 labelSize = new Vector2(
                        EditorGUIUtility.singleLineHeight * 2, EditorGUIUtility.singleLineHeight);
                Vector2 labelPos = HandleUtility.WorldToGUIPoint(pos);
                labelPos.y -= labelSize.y / 2;
                labelPos.x -= labelSize.x / 2;
                GUILayout.BeginArea(new Rect(labelPos, labelSize));
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.black;
                style.alignment = TextAnchor.MiddleCenter;
                GUILayout.Label(new GUIContent(i.ToString(), "Waypoint " + i), style);
                GUILayout.EndArea();
                Handles.EndGUI();
            }
        }

        void DrawPositionControl(int i)
        {
            CinemachinePath.Waypoint wp = Target.m_Waypoints[i];
            EditorGUI.BeginChangeCheck();
            Handles.color = Target.m_Appearance.pathColor;
            Quaternion rotation = (Tools.pivotRotation == PivotRotation.Local)
                ? Quaternion.identity : Quaternion.Inverse(Target.transform.rotation);
            float size = HandleUtility.GetHandleSize(wp.position) * 0.1f;
            Handles.SphereHandleCap(0, wp.position, rotation, size, EventType.Repaint);
            Vector3 pos = Handles.PositionHandle(wp.position, rotation);
            if (EditorGUI.EndChangeCheck())
            {
                if (i == 0)
                    Target.beginPath = wp.position;

                if (i == 1)
                    Target.endPath = wp.position;

                Undo.RecordObject(target, "Move Waypoint");
                wp.position = pos;
                Target.m_Waypoints[i] = wp;
            }
        }

        [DrawGizmo(GizmoType.Active | GizmoType.NotInSelectionHierarchy
             | GizmoType.InSelectionHierarchy | GizmoType.Pickable, typeof(CinemachinePath))]
        internal static void DrawPathGizmos(CinemachinePath path, GizmoType selectionType)
        {
            // Draw the path
            Color colorOld = Gizmos.color;
            Gizmos.color = (Selection.activeGameObject == path.gameObject)
                ? path.m_Appearance.pathColor : path.m_Appearance.inactivePathColor;
            float step = 1f;
            Vector3 lastPos = path.EvaluatePosition(path.MinPos);
            Vector3 lastW = (path.EvaluateOrientation(path.MinPos)
                             * Vector3.right) * path.m_Appearance.width / 2;
            for (float t = path.MinPos + step; t <= path.MaxPos + step / 2; t += step)
            {
                Vector3 p = path.EvaluatePosition(t);
                Quaternion q = path.EvaluateOrientation(t);
                Vector3 w = (q * Vector3.right) * path.m_Appearance.width / 2;
                Vector3 w2 = w * 1.2f;
                Vector3 p0 = p - w2;
                Vector3 p1 = p + w2;
                Gizmos.DrawLine(p0, p1);
                Gizmos.DrawLine(lastPos - lastW, p - w);
                Gizmos.DrawLine(lastPos + lastW, p + w);
                lastPos = p;
                lastW = w;
            }
            Gizmos.color = colorOld;
        }
    }
}
