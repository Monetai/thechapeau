﻿using UnityEditor;
using UnityEngine;
using System;

namespace Cinemachine.Editor
{
    [CustomEditor(typeof(CinemachineBaseRoom))]
    internal sealed class CinemachineBaseRoomEditor : UnityEditor.Editor
    {
        private CinemachineBaseRoom Target { get { return target as CinemachineBaseRoom; } }
        private static readonly string[] m_excludeFields = new string[] { "m_Script" };

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, m_excludeFields);
            serializedObject.ApplyModifiedProperties();
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Active, typeof(CinemachineBaseRoom))]
        private static void DrawTrackeDollyGizmos(CinemachineBaseRoom target, GizmoType selectionType)
        {
            if (target.IsValid)
            {
                CinemachineBaseRoomPath path = (CinemachineBaseRoomPath)target.m_Path;

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(target.m_Path.transform.position + path.beginPath, 0.2f);
                Gizmos.color = Color.cyan;
                Gizmos.DrawSphere(target.m_Path.transform.position + path.endPath, 0.2f);

                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(target.m_Path.transform.position + target.VirtualCamera.Follow.position, 0.2f);

                //Gizmos.color = Color.green;
                //Gizmos.DrawSphere(target.ProjectOnLine(target.m_Path.beginPath, target.m_Path.endPath, target.VirtualCamera.Follow), 0.2f);

                Gizmos.DrawRay(target.m_Path.transform.position + path.endPath, (target.m_Path.transform.position + path.beginPath) - (target.m_Path.transform.position + path.endPath));

                Vector3 vectorLeft = Quaternion.AngleAxis(-target.xAngle, Vector3.up) * Vector3.forward;
                Vector3 vectorRight = Quaternion.AngleAxis(target.xAngle, Vector3.up) * Vector3.forward;

                Vector3 originLeft = target.VirtualCamera.VirtualCameraGameObject.transform.position;
                Vector3 originRight = target.VirtualCamera.VirtualCameraGameObject.transform.position;

                Gizmos.color = Color.red;

                Gizmos.DrawRay(originLeft, vectorLeft.normalized * (target.rayDistance));
                Gizmos.DrawRay(originRight, vectorRight.normalized * (target.rayDistance));

                Gizmos.DrawSphere(target.VirtualCamera.VirtualCameraGameObject.transform.position + vectorRight.normalized * target.rayDistance, 0.2f);
                Gizmos.DrawSphere(target.VirtualCamera.VirtualCameraGameObject.transform.position + vectorLeft.normalized * target.rayDistance, 0.2f);
            }
        }
    }
}
