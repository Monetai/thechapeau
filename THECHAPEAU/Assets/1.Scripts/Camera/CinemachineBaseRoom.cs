﻿using UnityEngine;
using System;

namespace Cinemachine
{
    /// <summary>
    /// A Cinemachine Virtual Camera Body component that constrains camera motion
    /// to a CinemachinePath.  The camera can move along the path.
    /// 
    /// This behaviour can operate in two modes: manual positioning, and Auto-Dolly positioning.  
    /// In Manual mode, the camera's position is specified by animating the Path Position field.  
    /// In Auto-Dolly mode, the Path Position field is animated automatically every frame by finding
    /// the position on the path that's closest to the virtual camera's Follow target.
    /// </summary>
    [AddComponentMenu("")] // Don't display in add component menu
    [RequireComponent(typeof(CinemachinePipeline))]
    [SaveDuringPlay]
    public class CinemachineBaseRoom : CinemachineTrackedDolly
    {
        public float xAngle = 45;
        public float rayDistance = 5;
        public float lerpSpeed = 5f;
        public float targetDist = 10f;

        public bool followZ = true;
        public bool followY = true;

        public override void MutateCameraState(ref CameraState curState, float deltaTime)
        {
            base.MutateCameraState(ref curState, deltaTime);

            if (!IsValid)
                return;

            Vector3 position = curState.RawPosition;
            float basePos = curState.RawPosition.z;
            float interpolation = lerpSpeed * Time.deltaTime;

            if(deltaTime > 0)
            {
                if (followZ)
                {
                    position.z = Mathf.Lerp(this.transform.position.z, VirtualCamera.Follow.transform.position.z - targetDist, interpolation);
                    position.z = Mathf.Clamp(position.z, basePos, float.PositiveInfinity);
                }

                if (followY)
                {
                    position.y = Mathf.Lerp(this.transform.position.y, VirtualCamera.Follow.transform.position.y + 2, interpolation);
                }
            }
            else
            {
                if (followZ)
                {
                    position.z = VirtualCamera.Follow.transform.position.z - targetDist;
                    position.z = Mathf.Clamp(position.z, basePos, float.PositiveInfinity);
                }

                if (followY)
                {
                    position.y = VirtualCamera.Follow.transform.position.y + 2;
                }
            }

            curState.RawPosition = position;
        }
    }
}
