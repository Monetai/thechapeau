﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineBaseRoomPath : Cinemachine.CinemachinePath{

    public Vector3 beginPath;
    public Vector3 endPath;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + beginPath, 0.2f);
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position + endPath, 0.2f);

        //Gizmos.color = Color.green;
        //Gizmos.DrawSphere(target.ProjectOnLine(target.m_Path.beginPath, target.m_Path.endPath, target.VirtualCamera.Follow), 0.2f);
        Gizmos.DrawRay(transform.position + endPath, (transform.position + beginPath) - (transform.position + endPath));
    }
}
