﻿using UnityEngine;
using System;
using Cinemachine.Utility;

namespace Cinemachine
{
    /// <summary>
    /// This is a CinemachineComponent in the Aim section of the component pipeline.
    /// Its job is to aim the camera at the vcam's LookAt target object, with 
    /// configurable offsets, damping, and composition rules.
    /// 
    /// The composer does not change the camera's position.  It will only pan and tilt the 
    /// camera where it is, in order to get the desired framing.  To move the camera, you have
    /// to use the virtual camera's Body section.
    /// </summary>
    [DocumentationSorting(3, DocumentationSortingAttribute.Level.UserRef)]
    [ExecuteInEditMode] // for OnGUI
    [AddComponentMenu("")] // Don't display in add component menu
    [RequireComponent(typeof(CinemachinePipeline))]
    [SaveDuringPlay]
    public class CinemachineBaseRoomComposer : CinemachineComponentBase
    {
        [Range(-360,360)]
        Quaternion baseOrientation;
        public float rotationY;
        /// <summary>True if component is enabled and has a LookAt defined</summary>
        public override bool IsValid { get { return enabled && VirtualCamera.LookAt != null; } }

        /// <summary>Get the Cinemachine Pipeline stage that this component implements.
        /// Always returns the Aim stage</summary>
        public override CinemachineCore.Stage Stage { get { return CinemachineCore.Stage.Aim; } }

        private void OnEnable()
        {
            baseOrientation = Quaternion.Euler(rotationY,0,0);
        }

        public override void MutateCameraState(ref CameraState curState, float deltaTime)
        {
            if (!Application.isPlaying)
            {
                baseOrientation = Quaternion.Euler(rotationY, 0, 0);
            }

            curState.RawOrientation = baseOrientation;
        }
    }
}
