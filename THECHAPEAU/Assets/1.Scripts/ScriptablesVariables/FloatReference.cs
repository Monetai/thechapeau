﻿using UnityEngine;

namespace LittleWitch
{
    [System.Serializable]
    public class FloatReference : ScriptableVariableReference<float,FloatVariable>
    {
        public FloatReference()
        { 
        }
    }

    [System.Serializable]
    public class BoolReference : ScriptableVariableReference<bool,BoolVariable>
    {
        public BoolReference()
        {
        }
    }
}