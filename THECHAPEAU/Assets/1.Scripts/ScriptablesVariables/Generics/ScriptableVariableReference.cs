﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    [System.Serializable]
    public class ScriptableVariableReference<T,T2> : ScriptableVariableReferenceBase where T2 : ScriptableVariable<T>
    {
        public bool UseConstant = true;
        public T ConstantValue;
        public T2 Variable;

        public ScriptableVariableReference()
        { }

        public ScriptableVariableReference(T value)
        {
            UseConstant = true;
            ConstantValue = value;
        }

        public T Value
        {
            get { return UseConstant ? ConstantValue : Variable.CurrentValue; }
        }

        public static implicit operator T(ScriptableVariableReference<T,T2> reference)
        {
            return reference.Value;
        }
    }
}
