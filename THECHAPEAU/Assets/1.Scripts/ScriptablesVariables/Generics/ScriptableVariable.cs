﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    [System.Serializable]
    public class ScriptableVariable<T> : ScriptableVariableBase
    {
#if UNITY_EDITOR
        [Multiline]
        [ValidateInput("HasADescription", "No comment? Really?", InfoMessageType.Warning)]
        public string Comment = "";

        public bool HasADescription(string comment)
        {
            return !string.IsNullOrEmpty(comment);
        }
#endif

        [SerializeField]
        protected T DefaultValue;
        protected T currentValue;

        public T CurrentValue
        {
            get { return currentValue; }
            set { currentValue = value; }
        }

        private void OnEnable()
        {
            currentValue = DefaultValue;
        }

        public void SetValue(T value)
        {
            currentValue = value;
        }

        public void SetValue(ScriptableVariable<T> value)
        {
            currentValue = value.CurrentValue;
        }
    }
}
