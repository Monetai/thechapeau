﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    [CreateAssetMenu(menuName = "LittleWitch/RuntimeSets/create new ToolDataSet")]
    public class ToolDataSet : RuntimeSet<WitchToolData> {

    }
}
