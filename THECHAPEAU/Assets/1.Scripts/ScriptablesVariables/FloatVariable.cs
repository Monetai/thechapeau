﻿using UnityEngine;

namespace LittleWitch
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "LittleWitch/Variable/create new FloatVariable")]
    public class FloatVariable : ScriptableVariable<float>
    {
        
    }

    [System.Serializable]
    [CreateAssetMenu(menuName = "LittleWitch/Variable/create new BoolVariable")]
    public class BoolVariable : ScriptableVariable<bool>
    {

    }
}