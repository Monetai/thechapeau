﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LittleWitch {
    public class WwiseVolAux : MonoBehaviour {

        public GameObject gameObjSound;
        public string auxName;
        public float auxValue;

        public void OnTriggerStay(Collider other)
        {
            if (other.tag == "Player")
            {
                AkAuxSendArray aEnvs = new AkAuxSendArray();
                aEnvs.Add(gameObjSound, AkSoundEngine.GetIDFromString(auxName), auxValue);
                AkSoundEngine.SetGameObjectAuxSendValues(gameObjSound, aEnvs, 1);
            }
        }
    }
}