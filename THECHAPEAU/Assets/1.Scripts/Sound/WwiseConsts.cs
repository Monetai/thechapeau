namespace LittleWitch {

	public static class WwiseConstsInfos {
		public const string pathToWIFEvents = "Assets/4.Sound/Resources/WIFEvents";
	}

	public static class WIFConstsEvents {
		public const string TEST_AMB_EXT_01 = "TEST_AMB_EXT_01";
		public const string TEST_AMB_INT_01 = "TEST_AMB_INT_01";
		public const string TEST_BIP = "TEST_BIP";
		public const string TEST_MUSIC = "TEST_MUSIC";
		public const string TEST_SNOOP = "TEST_SNOOP";
		public const string TEST_STATE_01 = "TEST_STATE_01";
		public const string TEST_SWITCH_01 = "TEST_SWITCH_01";
		public const string TEST_VO = "TEST_VO";
	}

	public static class WwiseConstsBanks {
		public const string INIT = "INIT";
		public const string CHARACTER = "CHARACTER";
		public const string LEVEL1 = "LEVEL1";
		public const string MUSIC = "MUSIC";
		public const string PHYSIC = "PHYSIC";
		public const string SHARED = "SHARED";
		public const string TEST = "TEST";
		public const string VO = "VO";
	}

	public static class WwiseConstsBusses {
		public const string AMB = "AMB";
		public const string MASTER_AUDIO_BUS = "MASTER_AUDIO_BUS";
		public const string MASTER_SECONDARY_BUS = "MASTER_SECONDARY_BUS";
		public const string SFX = "SFX";
		public const string VO = "VO";
	}

	public static class WwiseConstsRTPC {
		public const string GAMEPARAM01 = "GAMEPARAM01";
	}

	public static class WwiseConstsTriggers {
		public const string TRIGGER01 = "TRIGGER01";
	}

	public static class WwiseConstsSwitches {
		public struct MATERIAL {
			public const string SWITCHGROUPNAME = "MATERIAL";
			public const uint SWITCHGROUPID = 3865314626;
			public const uint GROUND = 2528658256;
			public const uint METAL = 2473969246;
			public const uint WOOD = 2058049674;
		}
		public struct TYPE {
			public const string SWITCHGROUPNAME = "TYPE";
			public const uint SWITCHGROUPID = 2970581085;
			public const uint HARD = 3599861390;
			public const uint SCHWEPPS = 1481715236;
			public const uint SOFT = 670602561;
			public const uint SWEET = 2237492935;
		}
	}

	public static class WwiseConstsStates {
		public struct WATER {
			public const string STATEGROUPNAME = "WATER";
			public const uint STATEGROUPID = 2654748154;
			public const uint COLD = 3687161267;
			public const uint FREEZY = 3285814652;
		}
		public struct WEATHER {
			public const string STATEGROUPNAME = "WEATHER";
			public const uint STATEGROUPID = 317282339;
			public const uint CLOUDY = 3464232709;
			public const uint SUNNY = 3569642402;
		}
	}

}
