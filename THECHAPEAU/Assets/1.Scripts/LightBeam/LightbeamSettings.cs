using UnityEngine;
using System.Collections;

namespace LittleWitch
{
    public class LightbeamSettings : ScriptableObject
    {
        public float RadiusTop = 1;
        public float RadiusBottom = 5;
        public float Length = 10;
        public int Subdivisions = 25;
        public int SubdivisionsHeight = 6;
    }
}