using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class CrossBrightness : MonoBehaviour
    {
        public float brightness = 5;
        // Update is called once per frame
        void Update()
        {
            float dot = Vector3.Dot(transform.forward, (Camera.main.transform.position - transform.position).normalized);
            GetComponent<LensFlare>().brightness = Mathf.Clamp01(dot) * brightness;
        }
    }
}