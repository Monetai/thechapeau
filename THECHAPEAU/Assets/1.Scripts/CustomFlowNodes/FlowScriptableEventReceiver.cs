﻿using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using System.Collections;
using UnityEngine.Serialization;
using System.Collections.Generic;
using UnityEngine;
using System;
using NodeCanvas;
using NodeCanvas.Framework;
using com.ootii.Messages;

namespace LittleWitch
{
	
	public class FlowScriptableEventReceiver : EventNode 
	{
		public ValueInput<bool> canReceiveEvent;
		public ValueInput<EventAsset> eventToListen;
		private FlowOutput outEventReceived;

		protected override void RegisterPorts() 
		{
			base.RegisterPorts ();
			outEventReceived = AddFlowOutput ("OnEventReceived");
			canReceiveEvent = AddValueInput<bool>("CanReceiveEvent");
			eventToListen = AddValueInput<EventAsset>("EventToListen");

			canReceiveEvent.serializedValue = true;
		}

		public override void OnGraphStarted() {
			base.OnGraphStarted();

			if (eventToListen.value != null) {
				MessageDispatcher.AddListener (eventToListen.value.eventName, OnEventReceived);
			}
		}

		public override void OnGraphStoped() {
			base.OnGraphStoped ();
			if (eventToListen.value != null) {
				MessageDispatcher.RemoveListener (eventToListen.value.eventName, OnEventReceived);
			}
		}

		#region Callbacks
		private void OnEventReceived(IMessage mess) 
		{
			if(canReceiveEvent.value == true)
				outEventReceived.Call(new Flow());
		}
		#endregion
	}
}
