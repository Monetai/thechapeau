﻿using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using System.Collections;
using UnityEngine.Serialization;
using System.Collections.Generic;
using UnityEngine;
using System;
using NodeCanvas;
using NodeCanvas.Framework;
using com.ootii.Messages;

namespace LittleWitch
{
	[Category("LittleWitch/Events")]
	public class FlowScriptableEventSender : CallableActionNode<EventAsset>{

		//public BBParameter<EventAsset> eventToSend;

		public override void Invoke(EventAsset eventToSend)
		{
			if (eventToSend != null) 
			{
				MessageDispatcher.SendMessage (eventToSend.eventName);
			}
		}
	}
}