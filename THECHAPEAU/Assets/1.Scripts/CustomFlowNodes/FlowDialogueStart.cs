﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FlowCanvas.Nodes;
using FlowCanvas;
using ParadoxNotion.Design;
using NodeCanvas.DialogueTrees;

namespace LittleWitch
{
    [Name("Start Dialogue")]
    [Category("LittleWitch/Dialogues")]
    [Description("Start a dialogue tree.")]
    public class FlowDialogueStart : FlowControlNode
    {
        public DialogueTreeController dialogueTree;
        UiDialogManager dialogueManager;

        FlowOutput dialogueEnd;
        FlowInput flowIn;

        public override void OnGraphStarted()
        {
            base.OnGraphStarted();

            if(dialogueManager == null)
                dialogueManager = GameObject.FindObjectOfType<UiDialogManager>();

            if (dialogueTree == null)
                dialogueTree = graphAgent.GetComponent<DialogueTreeController>();
        }

        protected override void RegisterPorts()
        {
            flowIn = AddFlowInput("", (f) =>
            {


                if (dialogueManager == null)
                    dialogueManager = GameObject.FindObjectOfType<UiDialogManager>();

                if (dialogueTree != null && dialogueManager != null)
                {
                    DialogueActor actor = GameplayManager.Instance.currentPlayer.GetComponent<DialogueActor>();
                    if (actor && dialogueTree.GetActorReferenceByName(actor.name) == null)
                    {
                        dialogueTree.SetActorReference(actor.name, actor);
                    }

                    dialogueManager.OnDialogueFinished += OnDialogueEnd;
                    dialogueManager.PlayDialogue(dialogueTree);
                    InputProvider.Instance.SetMapState(false, RewiredConsts.Category.Default);
                    InputProvider.Instance.SetMapState(true, RewiredConsts.Category.UI);
                }
            });

            dialogueEnd = AddFlowOutput("OnDialogueEnd");
        }

        void OnDialogueEnd()
        {
            InputProvider.Instance.SetMapState(true, RewiredConsts.Category.Default);
            InputProvider.Instance.SetMapState(false, RewiredConsts.Category.UI);
            dialogueEnd.Call(new FlowCanvas.Flow());
            dialogueManager.OnDialogueFinished -= OnDialogueEnd;
        }
    }
}
