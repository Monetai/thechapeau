﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    [RequireComponent(typeof(StateController))]
    public abstract class State : MonoBehaviour
    {
        [BoxGroup("State Manager Setting",false)]
        public string id;

        [BoxGroup("State Manager Setting", false)]
        [SerializeField]
        private int priority;

        public int Priority
        {
            get { return priority; }
            set
            {
                if (value == priority)
                    return;

                priority = value;

                if (controller == null)
                {
                    Debug.LogError("State : " + name + " can't inform his controller of " +
                    "priority change since the controller is null !");
                    return;
                }

                controller.OnStatePriorityChanged(this);
            }
        }

        /// <summary>
        /// Our controller. Keep in mind that it is set up at the controller's Awake
        /// </summary>
        protected StateController controller;

        public StateController Controller
        {
            get { return controller; }
            set
            {
                controller = value;
            }
        }

        /// <summary>
        /// Is this State enabled ? (aka, can it be picked by the StateController)
        /// </summary>
        /// <value><c>true</c> if this instance is enabled; otherwise, <c>false</c>.</value>
        public virtual bool IsEnabled
        {
            get { return enabled; }
        }

        #region UnityCallbacks

        // From now, all the basic unity callbacks are overrided just in case
        // we need that later so we dont have to go modify all States
        protected virtual void Awake() { }

        protected virtual void Start() { }

        protected virtual void Update() { }

        #endregion

        #region State

        /// <summary>
        /// Determines whether this instance can become the current 
        /// controller State. You can set this to false if you are the
        /// currrent State and you want to stop.
        /// Checked every frame as long as the state is enabled
        /// </summary>
        public abstract bool CanBeActive();

        /// <summary>
        /// Determines whether this instance can be skipped.
        /// This should only called when this State is the current State.
        /// Default is yes.
        /// </summary>
        public virtual bool CanBeSkipped() { return true; }

        #endregion

        #region ControllerCallbacks

        /// <summary>
        /// Called when this State become the current State of the
        /// controller. Its a good time to register to events.
        /// </summary>
        public virtual void OnBecameCurrentState(State previousState) { }

        /// <summary>
        /// Called when this State is no longer the current State of
        /// the controller. Its a good time to unregister to events.
        /// </summary>
        public virtual void OnStateEnded() { }

        /// <summary>
        /// It's time to update the State. At this point, it's safe to 
        /// consider this State as the current State of the controller
        /// </summary>
        public virtual void OnUpdateState() { }

        #endregion

        public override string ToString()
        {
            return string.Format("State : ({0}) id = {1}", GetType().ToString(), id);
        }
    }
}