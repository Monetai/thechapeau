﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    /// <summary>
    /// really basic movement controller
    /// </summary>
    public class PhysicsController : MonoBehaviour
    {
        public float rotationSpeed = 180;
        public Animator animator;
        private Vector3 velocity = Vector3.zero;
        private Vector3 gravity = Vector3.zero;
        private Vector3 moveVec = Vector3.zero;

        private CharacterController controller;
        private float currentSpeed;
        private bool isGrounded;

        // Use this for initialization
        void Start()
        {
            controller = GetComponent<CharacterController>();
        }

        public void SetVelocity(Vector3 newVel)
        {
            velocity = newVel;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            moveVec = animator.deltaPosition;

            isGrounded = controller.isGrounded;
            if (isGrounded)
            {
                gravity.y = 0f;
            }
            else
            {
                gravity.y += (0.05f*Physics.gravity.y) * Time.deltaTime;
            }

            controller.Move(moveVec + velocity + gravity);
            transform.rotation *= animator.deltaRotation;
        }

        public void RotateTo(Vector3 wordlSpaceRotation)
        {
            if (!enabled)
                return;

            float angle = Mathf.Atan2(wordlSpaceRotation.x, wordlSpaceRotation.z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, angle, 0)), Time.deltaTime * rotationSpeed);
        }

        /// <summary>
        /// Return the angle between 0° and 360°
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        float NormaliseAngle(float angle)
        {
            angle %= 360;

            if (angle < 0)
            {
                angle += 360;
            }

            return angle;
        }

        public float GetAngleFromForward(Vector3 direction)
        {
            Vector3 axis = Vector3.Cross(transform.forward, direction);
            return Vector3.Angle(transform.forward, direction) * (axis.y < 0 ? -1 : 1);
        }
    }
}