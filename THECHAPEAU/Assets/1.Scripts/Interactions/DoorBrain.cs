﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    [RequireComponent(typeof(Door))]
    public class DoorBrain : InteractionBrain
    {
        public Transform doorElement;

        private Transform player;
        private Door door;

        private void Awake()
        {
            door = GetComponent<Door>();
        }

        private void Start()
        {
            player = GameplayManager.Instance.currentPlayer.transform;
        }

        protected override void OnInteractionBegin(BaseInteraction interaction)
        {
            base.OnInteractionBegin(interaction);

            if (door.GetInactiveRoom() == null)
            {
                return;
            }

            float side = door.GetSideFromDoor(player);
            player.GetComponent<PhysicsController>().enabled = false;
            door.ActivateBothRoom();
            MessageDispatcher.SendMessage(RoomsEvents.OnDoorOpened);

            player.DOMove(transform.position + (transform.forward * side), 0.8f).OnUpdate(() =>
            {
                player.transform.LookAt(transform.position);
            }).OnComplete(() =>
            {
                player.DOMove(transform.position + (transform.forward * 2 * -side), 2f).OnComplete(() =>
                {
                    player.GetComponent<PhysicsController>().enabled = true;
                });

                doorElement.DORotate(transform.rotation.eulerAngles + new Vector3(0, 90 * side, 0), 1.5f).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
                {
                    door.NotifyRooms();
                    MessageDispatcher.SendMessage(RoomsEvents.OnDoorClosed);
                    CompleteInteraction();
                });
            });
        }

        public override bool CanBeginInteraction()
        {
            return door.CanPassThrough();
        }
    }
}