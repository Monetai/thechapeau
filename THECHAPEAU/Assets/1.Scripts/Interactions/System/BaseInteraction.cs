﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
namespace LittleWitch
{
    public abstract class BaseInteraction : MonoBehaviour
    {
        public event System.Action<BaseInteraction> OnInteractionBegin;
        public event System.Action<BaseInteraction> OnInteractionDone;
        public event System.Action<BaseInteraction> OnInteractionCanceled;
        public event System.Action<BaseInteraction> OnInteractionClosed;
        public event System.Action<BaseInteraction> OnInteractionUpdate;

        protected bool isInteracting = false;
        public bool IsInteracting
        {
            get { return isInteracting; }
        }
        protected bool interactionCompleted = false;

        public bool isInstantInteraction;

        [Header("Interaction settings")]
        public bool onlyOnce = false;

        [HideIf("isInstantInteraction")]
        public InteractionBrain interactionBrain;

        private void Awake()
        {
            interactionBrain = GetComponent<InteractionBrain>();
            if (interactionBrain != null && isInstantInteraction == false)
            {
                interactionBrain.SetInteraction(this);
            }
        }

        public void UpdateInteraction()
        {
            if(OnInteractionUpdate != null)
                OnInteractionUpdate.Invoke(this);
        }

        /// <summary>
        /// Try to begin the interaction.
        /// </summary>
        public bool TryBeginInteraction()
        {
            if (InteractionIsReady())
            {
                OnBeginInteraction();
                return true;
            }
            else
                return false;
        }

        public virtual bool InteractionIsReady()
        {
            if (!enabled)
                return false;

            // If we are supposed to play only once but we already interacted
            if (onlyOnce && interactionCompleted)
            {
                return false;
            }

            if ((isInstantInteraction == false && interactionBrain != null && interactionBrain.CanBeginInteraction() == false))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Cancel an interaction
        /// </summary>
        public void CancelInteraction()
        {
            // We are not interacting
            if (!isInteracting)
                return;

            // If brain cant be stopped
            if ((isInstantInteraction == false && interactionBrain != null && interactionBrain.CanBeStopped() == false))
                return;

            OnCancelInteraction();
        }

        protected virtual void OnBeginInteraction()
        {
            isInteracting = true;

            if (OnInteractionBegin != null)
            {
                OnInteractionBegin(this);
            }

            if (isInstantInteraction == false && interactionBrain == null)
                CompleteInteraction();
        }

        /// <summary>
        /// Tells the interaction that it's done !
        /// </summary>
        public void CompleteInteraction()
        {
            // We are not interacting
            if (!isInteracting)
                return;

            if (onlyOnce)
            {
                interactionCompleted = true;
            }

            OnCompleteInteraction();
        }

        protected virtual void OnCancelInteraction()
        {
            if (OnInteractionCanceled != null)
                OnInteractionCanceled(this);

            OnInteractionClose();
        }

        protected virtual void OnCompleteInteraction()
        {
            if (OnInteractionDone != null)
                OnInteractionDone(this);

            OnInteractionClose();
        }

        /// <summary>
        /// The interaction has ended (done or canceled)
        /// </summary>
        protected virtual void OnInteractionClose()
        {
            if (OnInteractionClosed != null)
                OnInteractionClosed.Invoke(this);

            isInteracting = false;
        }

        public bool IsInteractionCompleted()
        {
            return interactionCompleted;
        }

        public void SetCompleteInteraction(bool isComplete)
        {
            interactionCompleted = isComplete;
        }

        /// <summary>
        /// TODO : Complete reset
        /// </summary>
        public virtual void ResetInteraction()
        {
            interactionCompleted = false;

            if (isInteracting)
            {
                CancelInteraction();
            }

            if (isInstantInteraction == false && interactionBrain != null)
            {
                interactionBrain.ResetBrain();
            }
        }
    }
}