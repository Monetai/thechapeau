﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LittleWitch {
    public class InteractIcon : MonoBehaviour
    {
        public SpriteRenderer icon;
        public InteractionManager interactManager;
        Vector3 targetScale;

        private void Awake()
        {
            if (icon == null)
                icon = GetComponent<SpriteRenderer>();

            if (interactManager == null)
                interactManager = GetComponent<InteractionManager>();

            targetScale = icon.transform.localScale;
            icon.transform.localScale = Vector3.zero;
        }

        //remove tweens
        private void Update()
        {
            if(interactManager.IsInteracting() )
            {
                icon.transform.localScale = Vector3.zero;
            }
            else if (interactManager.CanInteract())
            {
                icon.transform.localScale = targetScale;
            }
            else if (!interactManager.CanInteract())
            {
                icon.transform.localScale = Vector3.zero;
            }
        }

    }
}