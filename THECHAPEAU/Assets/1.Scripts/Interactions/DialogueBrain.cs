﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;

namespace LittleWitch
{
    [RequireComponent(typeof(DialogueTreeController))]
    public class DialogueBrain : InteractionBrain
    {
        DialogueTreeController dialogueTree;
        UiDialogManager dialogueManager;

        public void Start()
        {
            dialogueManager = FindObjectOfType<UiDialogManager>();
            dialogueTree = GetComponent<DialogueTreeController>();
        }

        public override bool CanBeginInteraction()
        {
            return dialogueTree != null && dialogueManager != null;
        }

        protected override void OnInteractionBegin(BaseInteraction interaction)
        {
            base.OnInteractionBegin(interaction);

            Debug.Log("coucou");

            DialogueActor actor = GameplayManager.Instance.currentPlayer.GetComponent<DialogueActor>();
            if(actor && dialogueTree.GetActorReferenceByName(actor.name) == null)
            {
                dialogueTree.SetActorReference(actor.name, actor);
            }

            dialogueManager.OnDialogueFinished += DialogueFinished;
            dialogueManager.PlayDialogue(dialogueTree);
            InputProvider.Instance.SetMapState(false, RewiredConsts.Category.Default);
            InputProvider.Instance.SetMapState(true, RewiredConsts.Category.UI);
        }

        private void DialogueFinished()
        {
            dialogueManager.OnDialogueFinished -= DialogueFinished;
            CompleteInteraction();
        }

        protected override void OnInteractionClosed(BaseInteraction interaction)
        {
            base.OnInteractionClosed(interaction);
            InputProvider.Instance.SetMapState(true, RewiredConsts.Category.Default);
            InputProvider.Instance.SetMapState(false, RewiredConsts.Category.UI);
        }
    }
}