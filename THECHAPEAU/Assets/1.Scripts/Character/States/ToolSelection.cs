﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class ToolSelection : State
    {
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;

        protected override void Start()
        {
            base.Start();
        }

        public override bool CanBeActive()
        {
            return inputManager.GetCurrentInputSource().GetOpenMenuToolsButton();
        }

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
            WitchToolsManager.Instance.OpenToolsMenu();
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            WitchToolsManager.Instance.CloseToolsMenu();
        }
    }
}