﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class Move : State
    {
        [BoxGroup("Mandatory References")]
        public Animator animator;
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        [BoxGroup("Mandatory References")]
        public PhysicsController physicsController;

        private int speedId = 0;
        private int directionId = 0;

        protected override void Awake()
        {
            base.Awake();
            speedId = Animator.StringToHash("Speed");
            directionId = Animator.StringToHash("Direction");
        }

        protected override void Start()
        {
            base.Start();
        }

        #region StateCallbacks
        public override bool CanBeActive()
        {
            if (new Vector3(inputManager.GetCurrentInputSource().GetMoveHorizontal(), 0, inputManager.GetCurrentInputSource().GetMoveVertical()).magnitude > 0.1f)
                return true;
            else
                return false;
        }

        public override void OnUpdateState()
        {
            base.OnUpdateState();

            Vector3 stickDirection = new Vector3(inputManager.GetCurrentInputSource().GetMoveHorizontal(), 0, inputManager.GetCurrentInputSource().GetMoveVertical());

            if (stickDirection.magnitude > 1)
                stickDirection.Normalize();

            float speedVector = (inputManager.GetCurrentInputSource().GetRunButton()) ? 2 : stickDirection.magnitude;

            float direction = physicsController.GetAngleFromForward(stickDirection);

            AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(0);

            bool inTransition = animator.IsInTransition(0);
            //bool inIdle = state.IsName("Idle");
            bool inTurn = state.IsName("TurnOnSpot");
            //bool inWalkRun = state.IsName("WalkRun");

            float directionDampTime = inTurn || inTransition ? 1000000 : 0;
            float speedDamp = (inputManager.GetCurrentInputSource().GetRunButton()) ? 0.5f : 0.1f;

            animator.SetFloat(speedId, speedVector, speedDamp, Time.deltaTime);
            animator.SetFloat(directionId, direction, directionDampTime, Time.deltaTime);
             
            if (!inTurn)
            {
                physicsController.RotateTo(stickDirection);
            }
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            animator.SetFloat(directionId, 0);
        }

        public void StopMoving()
        {
            animator.SetFloat(speedId, 0);
        }

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
        }
        #endregion

        
    }
}