﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class UsingPower : State
    {
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        [BoxGroup("Mandatory References")]
        public PhysicsController physicsController;
        [BoxGroup("Mandatory References")]
        public Animator animator;

        [BoxGroup("State Settings")]
        MagicTool magicTool;
        [BoxGroup("State Settings")]
        public float rotationSpeed;

        private int speedId = 0;
        //private int directionId = 0;
        private Vector3 baseForward;
        private float previousRotSpeed = 30;

        // Use this for initialization
        protected override void Awake()
        {
            if(magicTool == null)
            {
                magicTool = GetComponent<MagicTool>();
                if(magicTool == null)
                {
                    magicTool = GetComponentInParent<MagicTool>();
                }
            }

            if (magicTool == null)
            {
                Debug.LogError("Cannot Find magicTool!");
            }
        }

        protected override void Start()
        {
            speedId = Animator.StringToHash("Speed");
            //directionId = Animator.StringToHash("Direction");
        }

        public override bool CanBeActive()
        {
            return inputManager && magicTool && inputManager.GetCurrentInputSource().GetFireButton();
        }

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
            baseForward = physicsController.transform.forward;
            magicTool.Execute();

            previousRotSpeed = physicsController.rotationSpeed;
            physicsController.rotationSpeed = rotationSpeed;
        }

        // Update is called once per frame
        public override void OnUpdateState()
        {
            base.OnUpdateState();
            if(animator.GetFloat(speedId) >= 0)
            {
                animator.SetFloat(speedId, 0, 0.3f, Time.deltaTime);
            }

            //debug move
            Vector3 moveVector = new Vector3(inputManager.GetCurrentInputSource().GetMoveHorizontal(),0, inputManager.GetCurrentInputSource().GetMoveVertical()).normalized;
            physicsController.SetVelocity(moveVector * 2 * Time.deltaTime);

            //rotate
            if(moveVector != Vector3.zero)
            {
                Vector3 rotation = (baseForward + moveVector).normalized;
                if (Vector3.Angle(rotation, baseForward) < 60)
                    physicsController.RotateTo(rotation);
            }
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            physicsController.SetVelocity(Vector3.zero);
            physicsController.rotationSpeed = previousRotSpeed;
        }
    }
}
