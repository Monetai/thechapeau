﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class LampAim : MonoBehaviour
    {
        public float angle = 45;

        public bool lissage = true;

        public float rotationSpeed = 10f;
        public float returnSpeed = 5f;
        private float currentSpeed;

        private Vector3 rotationTarget;
        private Vector3 currentRot;
        private Vector3 precRot;

        void Start()
        {
            rotationTarget = new Vector3();
            currentRot = new Vector3();
            currentSpeed = returnSpeed;
        }

        // Update is called once per frame
        void Update()
        {
            SetRotationTarget(new Vector3(Input.GetAxis("Vertical2"), Input.GetAxis("Horizontal2"), 0));

            if (rotationTarget.magnitude > 1)
                rotationTarget.Normalize();

            if (rotationTarget == Vector3.zero)
                currentSpeed = returnSpeed;
            else
                currentSpeed = rotationSpeed;

            if (lissage)
            {
                currentRot = rotationTarget;
                currentRot = (precRot + currentRot) / 2;
                precRot = currentRot;
            }
            else
            {
                currentRot = rotationTarget;
            }

            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(currentRot * angle), currentSpeed * Time.deltaTime);
        }

        public void SetRotationTarget(Vector3 rotation)
        {
            rotationTarget = rotation;
        }
    }
}