﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LittleWitch
{
    public class journey3 : MonoBehaviour
    {

        public Vector3 initPositionList = new Vector3();
        Vector3 waitPos = new Vector3();
        Quaternion initRotationList = new Quaternion();

        //bool rewindValues = false;

        Tween tween;
        float duration = 2f;
        //Ease ease = Ease.InElastic;

        void Awake()
        {
            initPositionList = transform.position;
            initRotationList = transform.rotation;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Invoke("moncul", 0.1f);
            }

            //if (Input.GetKeyDown("r"))
            //{
            //    makeSeq();

            //}
        }

        void moncul()
        {
            waitPos = transform.position;
        }

        public void makeSeq()
        {
            transform.GetComponent<Collider>().enabled = false;
            transform.GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().AddTorque(Vector3.up * 15);
            // Grab a free Sequence to use
            Sequence mySequencePos = DOTween.Sequence();
            // Add a movement tween at the beginning

            //move seq
            Tween up = transform.DOMove(new Vector3(waitPos.x, transform.position.y, waitPos.z), duration / 2).SetEase(Ease.InOutQuint);
            mySequencePos.Append(up);
            //mySequencePos.Append(transform.DOMove(waitPos, duration).SetEase(Ease.InOutQuint));
            //rot seq
            mySequencePos.Append(transform.DORotate(initRotationList.eulerAngles, duration / 5).SetEase(Ease.InOutQuint).OnComplete(
                () =>
                {
                    transform.GetComponent<Rigidbody>().isKinematic = true;
                    transform.GetComponent<Collider>().enabled = false;
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                }));
            mySequencePos.Append(transform.DOMove(initPositionList, duration / 5).SetEase(Ease.InOutQuint));
        }
    }
}