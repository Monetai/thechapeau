﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LittleWitch
{
    public class Conntroller : MonoBehaviour
    {

        public List<journey3> journeyList;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                journey3[] jour = GameObject.FindObjectsOfType<journey3>();
                journeyList = new List<journey3>(jour);
            }

            if (Input.GetKeyDown("r"))
            {
                SortList();
                StartCoroutine(LaunchAnims());
            }
        }

        void SortList()
        {
            journeyList = journeyList.OrderBy(x => x.initPositionList.y).ToList();
        }

        IEnumerator LaunchAnims()
        {
            foreach (journey3 jour in journeyList)
            {
                jour.makeSeq();
                yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            }
        }
    }
}