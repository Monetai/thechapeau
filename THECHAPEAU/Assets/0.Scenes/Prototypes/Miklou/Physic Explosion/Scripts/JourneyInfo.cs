﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class JourneyInfo : MonoBehaviour
    {

        List<Vector3> positionList = new List<Vector3>();
        List<Quaternion> rotationList = new List<Quaternion>();

        bool rewindValues = false;

        void Awake()
        {
            positionList.Add(transform.position);
            rotationList.Add(transform.rotation);
        }

        // Update is called once per frame
        void Update()
        {

            if (Input.GetKeyDown("r"))
            {
                Invoke("RewindValues", Random.Range(0, 1.5f));
            }

            if (!transform.GetComponent<Rigidbody>().isKinematic && !rewindValues)
            {
                RecordValues();
            }

            if (rewindValues && transform.GetComponent<Rigidbody>().isKinematic == true && positionList.Count > 0)
            {
                transform.position = positionList[positionList.Count - 1];
                positionList.RemoveAt(positionList.Count - 1);
                transform.rotation = rotationList[rotationList.Count - 1];
                rotationList.RemoveAt(rotationList.Count - 1);
            }
        }



        public void RecordValues()
        {
            positionList.Add(transform.position);
            rotationList.Add(transform.rotation);
        }

        public void RewindValues()
        {
            rewindValues = true;
            transform.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}