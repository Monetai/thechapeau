﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class Explosion : MonoBehaviour
    {

        public float radius = 5.0f;
        public float power = 10.0f;

        public List<Collider> colliderList = new List<Collider>();

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown("space"))
            {
                ExplosionDamage(transform.position);
                Debug.Log("BOOM");
            }

        }

        void ExplosionDamage(Vector3 explosionPos)
        {
            foreach (Collider col in Physics.OverlapSphere(explosionPos, radius))
            {
                if (col.GetComponent<Rigidbody>() != null)
                {
                    col.GetComponent<Rigidbody>().isKinematic = false;
                    col.GetComponent<Rigidbody>().AddExplosionForce(power, explosionPos, radius, 3.0F);
                    colliderList.Add(col);
                }
            }
        }

        void Rewind()
        {
            foreach (Collider col in colliderList)
            {
                col.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
}