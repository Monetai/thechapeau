﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LittleWitch
{
    public class journey4 : MonoBehaviour
    {

        public Transform littleWitch;
        Vector3 initialPos;
        Quaternion initialRot;
        int phase;
        float speed = 1f;
        float angle = 0;
        float radius;
       // float timer = 0;
        float maxTimer;
        float rotSpeed = 1;
        float maxY;
        float timer2 = 0;
        float limit;
        public void Start()
        {
            initialPos = transform.position;
            initialRot = transform.rotation;
            radius = (5 + (Random.Range(-0.5f, 0.5f)));
            maxTimer = 4 + (Random.Range(0f, 0.01f));
            maxY = 6 + (Random.Range(-0.5f, 0.5f));
            speed += Random.Range(-0.5f, 0.5f);

            limit = Random.Range(0, 0.5f);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                phase = 1;
                GetComponent<Collider>().enabled = false;
                GetComponent<Rigidbody>().useGravity = false;
                GetComponent<Rigidbody>().velocity = Vector3.zero;
            }

            switch (phase)
            {
                case 1:
                    if (Vector3.Distance(transform.position, littleWitch.position) < radius)
                    {
                        phase++;
                        angle = Vector3.Angle(littleWitch.position, transform.position);
                        DOTween.To(x => rotSpeed = x, 1, 15, maxTimer).SetEase(Ease.InOutSine);
                    }
                    else GoToWitch();
                    break;
                case 2:
                    if (Input.GetKeyDown(KeyCode.H) && phase == 2)
                    {

                        DOTween.To(x => radius = x, radius, 1, 0.5f).SetEase(Ease.OutExpo).OnComplete(() =>
                        {

                            DOTween.To(x => rotSpeed = x, rotSpeed, 0, 0.3f).SetEase(Ease.OutExpo).OnComplete(() =>
                            {
                                DOTween.To(x => rotSpeed = x, rotSpeed, -30, 0.5f).SetEase(Ease.OutExpo).OnComplete(() => { phase++; });
                            });

                        });
                    }
                    else TurnAroundWitch(); break;
                case 3:
                    GotoPos();
                    break;
                default:
                    break;
            }
        }

        public void GoToWitch()
        {
            Vector3 dir = littleWitch.position - transform.position;
            transform.position = transform.position + dir * speed * Time.deltaTime;
        }

        public void TurnAroundWitch()
        {
            angle += Time.deltaTime * rotSpeed;

            float posX = littleWitch.position.x + (Mathf.Cos(angle) * radius);
            float posZ = littleWitch.position.z + (Mathf.Sin(angle) * radius);
            float posY = transform.position.y + Time.deltaTime * 1.5f + Mathf.Sin(angle) * 0.05f;
            posY = Mathf.Clamp(posY, 0, maxY);
            transform.position = new Vector3(posX, posY, posZ);
        }

        public void GotoPos()
        {
            if (timer2 > limit)
            {
                phase++;

                transform.DOMove(initialPos, 0.35f).SetEase(Ease.InCubic).OnComplete(() =>
                {
                    GetComponent<Collider>().enabled = false;
                    GetComponent<Rigidbody>().useGravity = false;
                    GetComponent<Rigidbody>().isKinematic = true;
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                    transform.rotation = initialRot;
                });
            }
            else
            {
                TurnAroundWitch();
                timer2 += Time.deltaTime;
            }
        }

    }
}