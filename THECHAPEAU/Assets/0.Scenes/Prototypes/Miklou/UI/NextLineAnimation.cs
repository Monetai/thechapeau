﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LittleWitch
{
    public class NextLineAnimation : MonoBehaviour
    {

        Sequence seq;
        // Use this for initialization
        void Start()
        {
            seq = DOTween.Sequence();

            seq.Append(transform.DOMove(new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z), 0.7f));
            seq.Append(transform.DOMove(new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), 0.5f));
            seq.SetLoops(-1);
        }

        private void OnEnable()
        {
            seq.Play();
        }

        private void OnDisable()
        {
            seq.Rewind();
            seq.Pause();
        }
    }
}