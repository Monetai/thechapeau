﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using NodeCanvas.DialogueTrees;
using System;

namespace LittleWitch
{
    public class UiDialogManager : MonoBehaviour
    {
        public GameObject contentPanel, contentButton, choicePanel;
        public Button choiceButton;
        public TextMeshProUGUI contentChar, contentText;
        public bool inDialog;

        private DialogueTreeController dialogueController;
        private EventSystem eventSystem;
        private List<Button> listChoiceButtons = new List<Button>();

        public Action OnDialogueStarted, OnDialoguePaused, OnDialogueFinished;

        //---------------------------------------------------------------------
        // Use this for initialization
        void Start()
        {
            eventSystem = FindObjectOfType<EventSystem>();

            // initialize ui
            contentPanel.SetActive(false);
            contentButton.SetActive(false);

            // know if the dialog is launch
            inDialog = false;

        }

        //---------------------------------------------------------------------
        // Use this for enable
        void OnEnable()
        {
            DialogueTree.OnDialogueStarted += DialogueStarted;
            DialogueTree.OnDialoguePaused += DialoguePaused;
            DialogueTree.OnDialogueFinished += DialogueFinished;
            DialogueTree.OnSubtitlesRequest += SubtitlesRequest;
            DialogueTree.OnMultipleChoiceRequest += MultipleChoiceRequest;
        }

        //---------------------------------------------------------------------
        // Use this for disable
        void OnDisable()
        {
            DialogueTree.OnDialogueStarted -= DialogueStarted;
            DialogueTree.OnDialoguePaused -= DialoguePaused;
            DialogueTree.OnDialogueFinished -= DialogueFinished;
            DialogueTree.OnSubtitlesRequest -= SubtitlesRequest;
            DialogueTree.OnMultipleChoiceRequest -= MultipleChoiceRequest;
        }

        //---------------------------------------------------------------------
        // When dialog started
        private void DialogueStarted(DialogueTree obj)
        {
            contentPanel.SetActive(true);

            if (OnDialogueStarted != null)
                OnDialogueStarted.Invoke();
        }

        //---------------------------------------------------------------------
        // When dialog paused
        private void DialoguePaused(DialogueTree obj)
        {
            if (OnDialoguePaused != null)
                OnDialoguePaused.Invoke();
        }

        //---------------------------------------------------------------------
        // When dialog finished
        private void DialogueFinished(DialogueTree obj)
        {
            if (OnDialogueFinished != null)
                OnDialogueFinished.Invoke();

            dialogueController = null;
            // remove ui
            contentPanel.SetActive(false);
            contentButton.SetActive(false);
        }

        //---------------------------------------------------------------------
        // On subtitles request
        private void SubtitlesRequest(SubtitlesRequestInfo obj)
        {
            StartCoroutine(Internal_OnSubtitlesRequest(obj));
        }

        //---------------------------------------------------------------------
        // On multichoice request
        private void MultipleChoiceRequest(MultipleChoiceRequestInfo obj)
        {
            Internal_OnMultipleChoiceRequest(obj);
        }

        //---------------------------------------------------------------------
        // Coroutine for subtitle request
        IEnumerator Internal_OnSubtitlesRequest(SubtitlesRequestInfo obj)
        {
            // change content values
            contentText.text = obj.statement.text;
            contentChar.text = obj.actor.name;
            contentButton.SetActive(true);

            // avoid the previous selected choice problem
            yield return null;

            // wait for the good input
            while (InputProvider.Instance.GetActionDown(RewiredConsts.Action.UI.UISubmit) == false)
            {
                yield return null;
            }

            // continue
            contentButton.SetActive(false);

            yield return null;
            obj.Continue();
        }

        //---------------------------------------------------------------------
        // Coroutine for multiplechoice request
        void Internal_OnMultipleChoiceRequest(MultipleChoiceRequestInfo obj)
        {
            foreach (KeyValuePair<IStatement, int> pair in obj.options)
            {
				var btn = (Button)Instantiate(choiceButton,choicePanel.transform);
				btn.gameObject.SetActive(true);
				btn.GetComponentInChildren<TextMeshProUGUI>().text = pair.Key.text;
                listChoiceButtons.Add(btn);
                btn.onClick.AddListener(() => { Finalize(obj, pair.Value); });
            }

            // select the first button choice
            eventSystem.SetSelectedGameObject(listChoiceButtons[0].gameObject);
        }

        //---------------------------------------------------------------------
        // Validate a multiplechoice request
        void Finalize(MultipleChoiceRequestInfo obj, int index)
        {
            // destroy all instantiate buttons and hide the container
            foreach (var tempBtn in listChoiceButtons)
            {
                Destroy(tempBtn.gameObject);
            }
            listChoiceButtons.Clear();
            contentButton.SetActive(false);

            // select option and go to next
            obj.SelectOption(index);
        }

        public void PlayDialogue(DialogueTreeController tree)
        {
            dialogueController = tree;
            dialogueController.StartDialogue();
        }
    }
}