﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class ControllerTest : MonoBehaviour
    {

        public GameObject gameObjSound;

        private void Start()
        {
            WwiseWrapper.WIFLoadBank(WwiseConstsBanks.TEST);
        }
        
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.T)) // t comme test :D
            {
                WwiseWrapper.WIFPlayEvent(WIFConstsEvents.TEST_AMB_EXT_01, gameObjSound);
            }
        }
    }
}
