﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCircleGizmo : MonoBehaviour {

    public float radius = 3f;
    public int nbPoint = 4;
    public float angleToCover = 360f;
    private float delta = 0f;
    public float offset;

	// Update is called once per frame
	void OnDrawGizmos () {
        delta = angleToCover / nbPoint;
        for (int i = 0; i< nbPoint; i++)
        {
            float x = radius * Mathf.Cos((delta * i + offset) * Mathf.Deg2Rad);
            float y = radius * Mathf.Sin((delta * i + offset) * Mathf.Deg2Rad);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(new Vector3(x, y, 0),0.5f);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position + new Vector3(x, y, 0), 0.5f);
        }
    }
}
