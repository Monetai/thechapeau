﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LittleWitch
{
    [CreateAssetMenu(fileName = "ToolData", menuName = "LittleWitch/ToolData/create new ToolData", order = 1)]
    public class WitchToolData : ScriptableObject
    {
        public Color toolColor = Color.white;
        public Sprite toolSprite;
        public string toolName;
        public string toolDescription;
    }
}