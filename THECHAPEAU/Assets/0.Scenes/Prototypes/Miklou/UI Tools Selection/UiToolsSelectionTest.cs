﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class UiToolsSelectionTest : MonoBehaviour
    {

        public WitchToolsManager witchToolsManager;

        private void Start()
        {
            witchToolsManager.OpenToolsMenu();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                witchToolsManager.OpenToolsMenu();
            }
        }
    }
}