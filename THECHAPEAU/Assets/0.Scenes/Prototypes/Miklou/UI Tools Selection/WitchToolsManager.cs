﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

namespace LittleWitch
{
    public class WitchToolsManager : MonoBehaviour
    {
        public static WitchToolsManager Instance;
        public ToolDataSet avaiableTools;
        public Image toolImage;
        public GameObject toolPanel;
        public Image toolIconPrefab;
        public TextMeshProUGUI toolDescription;
        public Image toolCursor;
        public float radius = 3f;
        public float angleToCover = 360f;
        public float speed = 20;

        private List<WitchToolData> witchToolsData; // TEMP get the list of the character
        private List<KeyValuePair<WitchToolData, Image>> witchToolsData_internal; // TEMP get the list of the character
        //private WitchToolData currentTool;
        private float delta = 0f;
        private float nbTools = 0f;
        private float offset = 0f;
        private Vector3 lerpedInput;
        private Vector3 prevInput;

        private void Awake()
        {
            Instance = this;
        }

        //---------------------------------------------------------------------
        // Use this for initialization
        void Start()
        {
            // TEMP initialize list tools
            witchToolsData_internal = new List<KeyValuePair<WitchToolData, Image>>();
            // TEMP activate the menu
            GetComponent<Canvas>().enabled = false;
        }

        //---------------------------------------------------------------------
        // Update is called once per frame
        void Update()
        {
            if (GetComponent<Canvas>().enabled == false)
                return;

            // get the value of the pad
            Vector3 inputValue = new Vector3(InputProvider.Instance.GetAxis(RewiredConsts.Action.UI.UIHorizontal), InputProvider.Instance.GetAxis(RewiredConsts.Action.UI.UIVertical),0).normalized;

            if (inputValue.magnitude != 1)
            {
                inputValue = prevInput;
            }
            else prevInput = inputValue;

            lerpedInput = Vector3.RotateTowards(lerpedInput, inputValue, speed * Time.deltaTime, 100f).normalized;
            lerpedInput.z = 0;
            // get the direction of the pad
            Vector3 direction = (transform.position + lerpedInput * radius) - transform.position;

            // update the cursor position
            toolCursor.transform.position = (transform.position + lerpedInput * radius / 2);
            
            // convert direction to an angle
            float angle = Vector3.Angle(transform.right, direction);
            // get the tool selected
            int index = (int) Mathf.Clamp(Mathf.FloorToInt(angle / (angleToCover / nbTools)), 0, nbTools - 1);

            // on selected tools
            OnSelectedTools(witchToolsData_internal[index].Key);
        }

        //---------------------------------------------------------------------
        // when the menu is open
        public void OpenToolsMenu()
        {
            witchToolsData = avaiableTools.CurrentItems;

            if (toolIconPrefab == null)
            {
                Debug.LogError("There is no toolIconPrefab.");
                return;
            }

            MagicTool magicTool = GameObject.FindGameObjectWithTag("Player").GetComponent<MagicTool>();
            if (magicTool == null)
            {
                Debug.LogError("Can't find a magicTool.");
                return;
            }

            // change input provider
            InputProvider.Instance.SetMapState(true, RewiredConsts.Category.UI);
            //InputProvider.Instance.SetMapState(false, RewiredConsts.Category.Default);

            // activate the menu
            GetComponent<Canvas>().enabled = false;

            foreach (WitchToolData tool in witchToolsData)
            {
                Image img = Instantiate(toolIconPrefab, toolPanel.transform) as Image;
                witchToolsData_internal.Add(new KeyValuePair<WitchToolData, Image>(tool, img));
            }

            // display each tools on a circle
            nbTools = witchToolsData_internal.Count;
            offset = (angleToCover / nbTools) / 2;
            delta = angleToCover / nbTools;
            int index = 0;

            foreach (KeyValuePair<WitchToolData, Image> pair in witchToolsData_internal)
            {
                float x = radius * Mathf.Cos((delta * index + offset) * Mathf.Deg2Rad) + transform.position.x;
                float y = radius * Mathf.Sin((delta * index + offset) * Mathf.Deg2Rad) + transform.position.y;
                pair.Value.transform.position = new Vector3(x, y, pair.Value.transform.position.z);

                // set button for corresponding tools
                pair.Value.GetComponent<Image>().sprite = pair.Key.toolSprite;
                pair.Value.GetComponent<Image>().color = pair.Key.toolColor;
                //pair.Value.GetComponent<Image>().SetNativeSize();
                index++;
            }

            GetComponent<Canvas>().enabled = true;
        }

        //---------------------------------------------------------------------
        // when a tool is select
        public void OnSelectedTools(WitchToolData tool)
        {
            // update current tool
            //currentTool = tool;
            // update the description
            toolDescription.text = tool.toolDescription;
        }

        //---------------------------------------------------------------------
        // when the menu is exit
        public void CloseToolsMenu()
        {
            // destroy all instantiate images
            foreach (KeyValuePair<WitchToolData, Image> pair in witchToolsData_internal)
            {
                Destroy(pair.Value.gameObject);
            }
            witchToolsData_internal.Clear();
            // reset the description
            toolDescription.text = "";
            // desactivate the menu
            GetComponent<Canvas>().enabled = false;

            InputProvider.Instance.SetMapState(false, RewiredConsts.Category.UI);

        }
    }
}