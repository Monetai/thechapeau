﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using TMPro;

namespace LittleWitch
{
    public class ParseI2ToLetterV2 : MonoBehaviour
    {

        private TextMeshPro m_textMeshPro;

        // Use this for initialization
        void Start()
        {
            StartCoroutine("WaitToDisplayLetter");

            m_textMeshPro.text = ScriptLocalization.Get("Prot_Dialog03_Text01");
        }


        IEnumerator WaitToDisplayLetter()
        {
            // get reference to textmeshpro component if one exists; otherwise add one
            m_textMeshPro = gameObject.GetComponent<TextMeshPro>() ?? gameObject.AddComponent<TextMeshPro>();

            int totalVisibleCharacter = m_textMeshPro.textInfo.characterCount; // get # of visible character in text object
            int counter = 0;
            int visibleCount = 0;

            while (visibleCount < totalVisibleCharacter)
            {
                visibleCount = counter % (totalVisibleCharacter + 1);

                m_textMeshPro.maxVisibleCharacters = visibleCount; // how many characters should TextMeshPro display ?

                // one the last character has been revealed, wait 1.0 second and start over
                if (visibleCount >= totalVisibleCharacter)
                    yield return new WaitForSeconds(1.0f);

                counter += 1;

                yield return new WaitForSeconds(0.05f);
            }

            Debug.Log("lol c'est fini");

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}