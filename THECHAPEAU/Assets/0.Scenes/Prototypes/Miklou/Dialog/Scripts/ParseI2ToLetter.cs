﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;

namespace LittleWitch
{
    public class ParseI2ToLetter : MonoBehaviour
    {

        public UnityEngine.UI.Text textToDisplay;

        // Use this for initialization
        void Start()
        {
            StartCoroutine("WaitToDisplayLetter");
        }


        IEnumerator WaitToDisplayLetter()
        {
            string text1 = ScriptLocalization.Get("Prot_Dialog02_Text01");
            foreach (char letter in text1)
            {
                textToDisplay.text = textToDisplay.text + letter;
                yield return new WaitForSeconds(0.3f);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
    