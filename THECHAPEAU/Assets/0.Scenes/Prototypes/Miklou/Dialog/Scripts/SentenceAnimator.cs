﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace LittleWitch
{
    public class SentenceAnimator : MonoBehaviour
    {
        List<LetterAnimator> animators;

        public float timeBetweenLetters = 0.1f;

        private float duration = 0.7f;
        TMPro.TMP_Text text;

        public System.Action OnTextAnimationEnded;

        void Awake()
        {
            text = GetComponent<TMPro.TMP_Text>();
        }

        private void Start()
        {
            //StartAnimation();
        }

        public void StartAnimation()
        {
            StartCoroutine(AnimateText());
        }

        public void StartAnimation(string textToDisplay)
        {
            text.text = textToDisplay;
            StartCoroutine(AnimateText());
        }

        private IEnumerator AnimateText()
        {
            WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
            float timer = 0;
            int lastIndex = 0;
            animators = new List<LetterAnimator>();

            for (int i = 0; i < text.textInfo.characterCount; i++)
            {
                animators.Add(new LetterAnimator(duration, DG.Tweening.Ease.OutElastic));
            }

            while (true)
            {
                if (timer > duration + timeBetweenLetters && lastIndex >= text.textInfo.characterCount)
                {
                    if (OnTextAnimationEnded != null)
                        OnTextAnimationEnded.Invoke();
                    break;
                }

                timer += Time.deltaTime;
                if (timer > timeBetweenLetters && lastIndex < text.textInfo.characterCount)
                {
                    timer = 0;
                    // comment to stop anim and error //animators[lastIndex].PlayTween();
                    lastIndex++;
                }

                // comment to stop anim and error //SetCharSize();

                yield return waitFrame;
            }
        }

        private void SetCharSize()
        {
            text.ForceMeshUpdate();

            TMP_TextInfo textInfo = text.textInfo;

            Matrix4x4 matrix;
            TMP_MeshInfo[] cachedMeshInfoVertexData = textInfo.CopyMeshInfoVertexData();

            for (int i = 0; i < textInfo.characterCount; i++)
            {
                if (text.text[i] == ' ')
                    continue;

                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Get the index of the material used by the current character.
                int materialIndex = charInfo.materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = charInfo.vertexIndex;

                // Get the cached vertices of the mesh used by this text element (character or sprite).
                Vector3[] sourceVertices = cachedMeshInfoVertexData[materialIndex].vertices;

                // Determine the center point of each character.
                Vector2 charMidBasline = (sourceVertices[vertexIndex + 0] + sourceVertices[vertexIndex + 2]) / 2;

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                // This is needed so the matrix TRS is applied at the origin for each character.
                Vector3 offset = charMidBasline;

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;

                destinationVertices[vertexIndex + 0] = sourceVertices[vertexIndex + 0] - offset;
                destinationVertices[vertexIndex + 1] = sourceVertices[vertexIndex + 1] - offset;
                destinationVertices[vertexIndex + 2] = sourceVertices[vertexIndex + 2] - offset;
                destinationVertices[vertexIndex + 3] = sourceVertices[vertexIndex + 3] - offset;

                // Determine the random scale change for each character.
                //float randomScale = Random.Range(1f, 1.5f);

                // Setup the matrix for the scale change.
                matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, Vector3.one * animators[i].GetSize());

                destinationVertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 0]);
                destinationVertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 1]);
                destinationVertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 2]);
                destinationVertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 3]);

                destinationVertices[vertexIndex + 0] += offset;
                destinationVertices[vertexIndex + 1] += offset;
                destinationVertices[vertexIndex + 2] += offset;
                destinationVertices[vertexIndex + 3] += offset;


                // Restore Source UVS which have been modified by the sorting
                Vector2[] sourceUVs0 = cachedMeshInfoVertexData[materialIndex].uvs0;
                Vector2[] destinationUVs0 = textInfo.meshInfo[materialIndex].uvs0;

                destinationUVs0[vertexIndex + 0] = sourceUVs0[vertexIndex + 0];
                destinationUVs0[vertexIndex + 1] = sourceUVs0[vertexIndex + 1];
                destinationUVs0[vertexIndex + 2] = sourceUVs0[vertexIndex + 2];
                destinationUVs0[vertexIndex + 3] = sourceUVs0[vertexIndex + 3];

                // Restore Source Vertex Colors
                Color32[] sourceColors32 = cachedMeshInfoVertexData[materialIndex].colors32;
                Color32[] destinationColors32 = textInfo.meshInfo[materialIndex].colors32;

                destinationColors32[vertexIndex + 0] = sourceColors32[vertexIndex + 0];
                destinationColors32[vertexIndex + 1] = sourceColors32[vertexIndex + 1];
                destinationColors32[vertexIndex + 2] = sourceColors32[vertexIndex + 2];
                destinationColors32[vertexIndex + 3] = sourceColors32[vertexIndex + 3];

            }

            // Push changes into meshes
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                // Updated modified vertex attributes
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                textInfo.meshInfo[i].mesh.uv = textInfo.meshInfo[i].uvs0;
                textInfo.meshInfo[i].mesh.colors32 = textInfo.meshInfo[i].colors32;

                text.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }
        }
    }
}