﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace LittleWitch
{
    public class LetterAnimator
    {
        private float currentSize = 0f;
        private Tween tween;
        private float duration;
        private Ease ease;

        public LetterAnimator(float duration, Ease ease)
        {
            this.duration = duration;
            this.ease = ease;
        }

        public void PlayTween()
        {
            tween = DOTween.To(x => currentSize = x, 0f, 1f, duration).SetEase(ease);
        }

        public void StopTween()
        {
            tween.Kill();
        }

        public float GetSize()
        {
            return currentSize;
        }

    }
}