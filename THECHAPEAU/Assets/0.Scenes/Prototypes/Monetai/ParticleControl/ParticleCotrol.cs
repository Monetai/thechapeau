﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCotrol : MonoBehaviour 
{
	ParticleSystem particleSystem;
	public float width = 5;
	public float height = 5;
	ParticleSystem.Particle[] parts;
	// Use this for initialization
	public float range = 2;
	public GameObject target;

	void Start () {
		particleSystem = GetComponent<ParticleSystem> ();
		ParticleSystem.MainModule main = particleSystem.main;
		main.maxParticles = (int)(width * height);


		InitializeIfNeeded ();
		int nbPartsLive = particleSystem.GetParticles (parts);

		for (int i = 0; i < nbPartsLive; i++) 
		{
			parts [i].position = new Vector3 (i%5,transform.position.y, (int)(i/5));
		}

		particleSystem.SetParticles (parts, nbPartsLive);
	}
	
	// Update is called once per frame
	void Update () 
	{
		InitializeIfNeeded ();
		int nbPartsLive = particleSystem.GetParticles (parts);

		for (int i = 0; i < nbPartsLive; i++) 
		{
			parts [i].position = new Vector3 (i%5,parts [i].position.y, (int)(i/5));

			Vector3 targetPos = target.transform.position;
			targetPos.y = 0;
			Vector3 partPos = parts [i].position;
			partPos.y = 0;

			float t = Vector3.Distance (targetPos, transform.TransformPoint (partPos)) / range;
			if (t < 0.3f)
				t = 0;

			partPos.y = Mathf.Lerp (partPos.y, parts [i].position.y, t);
			partPos.y = transform.InverseTransformPoint (partPos).y;
			parts [i].position = partPos;
		}

		particleSystem.SetParticles (parts, nbPartsLive);
	}

	void InitializeIfNeeded()
	{
		if (parts == null || parts.Length < particleSystem.main.maxParticles)
			parts = new ParticleSystem.Particle[particleSystem.main.maxParticles];
	}
}
