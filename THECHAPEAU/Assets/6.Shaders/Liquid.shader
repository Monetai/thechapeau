﻿Shader "Liquid" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_SurfaceColor("Surface Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue" = "AlphaTest+100"}
		GrabPass{ }
		LOD 200
		Cull Front
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#pragma target 4.0

		struct Input 
		{
			float2 uv_MainTex;
			float3 localPos;
			INTERNAL_DATA
		};

		uniform sampler2D _GrabTexture;
		sampler2D _MainTex;
		float _SurfaceHeight;
		half _Glossiness;
		half _Metallic;
		fixed4 _SurfaceColor;
		float4 _Pendulum;
		float4 _SurfacePoint;

		void vert(inout appdata_full v, out Input o) 
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			v.normal = normalize(_SurfacePoint - _Pendulum);
			o.localPos = v.vertex.xyz;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			if(dot(normalize(IN.localPos - _SurfacePoint),normalize(_SurfacePoint - _Pendulum)) > 0)
			{
				discard;
			}
			o.Albedo = _SurfaceColor;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = _SurfaceColor.a;
		}
		ENDCG

		Cull Back
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#pragma target 4.0

		struct Input 
		{
			float2 uv_MainTex;
			float3 localPos;
			INTERNAL_DATA
		};

		uniform sampler2D _GrabTexture;
		sampler2D _MainTex;
		float _SurfaceHeight;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float4 _Pendulum = { 0,-1, 0, 0 };
		float4 _SurfacePoint = { 0, 0, 0, 0 };

		void vert(inout appdata_full v, out Input o) 
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.localPos = v.vertex.xyz;
		}

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			if (dot(normalize(IN.localPos - _SurfacePoint), normalize(_SurfacePoint - _Pendulum)) > 0)
			{
				discard;
			}
			o.Albedo = _Color;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = _Color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
