/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID TEST_AMB_EXT_01 = 112812782U;
        static const AkUniqueID TEST_AMB_INT_01 = 1176675860U;
        static const AkUniqueID TEST_BIP = 3530412377U;
        static const AkUniqueID TEST_MUSIC = 379521629U;
        static const AkUniqueID TEST_SNOOP = 3780279095U;
        static const AkUniqueID TEST_STATE_01 = 2419652527U;
        static const AkUniqueID TEST_SWITCH_01 = 548053102U;
        static const AkUniqueID TEST_VO = 1913153617U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace WATER
        {
            static const AkUniqueID GROUP = 2654748154U;

            namespace STATE
            {
                static const AkUniqueID COLD = 3687161267U;
                static const AkUniqueID FREEZY = 3285814652U;
            } // namespace STATE
        } // namespace WATER

        namespace WEATHER
        {
            static const AkUniqueID GROUP = 317282339U;

            namespace STATE
            {
                static const AkUniqueID CLOUDY = 3464232709U;
                static const AkUniqueID SUNNY = 3569642402U;
            } // namespace STATE
        } // namespace WEATHER

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MATERIAL
        {
            static const AkUniqueID GROUP = 3865314626U;

            namespace SWITCH
            {
                static const AkUniqueID GROUND = 2528658256U;
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace MATERIAL

        namespace TYPE
        {
            static const AkUniqueID GROUP = 2970581085U;

            namespace SWITCH
            {
                static const AkUniqueID HARD = 3599861390U;
                static const AkUniqueID SCHWEPPS = 1481715236U;
                static const AkUniqueID SOFT = 670602561U;
                static const AkUniqueID SWEET = 2237492935U;
            } // namespace SWITCH
        } // namespace TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID GAMEPARAM01 = 902715719U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID TRIGGER01 = 3969765746U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID LEVEL1 = 2678230382U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID PHYSIC = 359588701U;
        static const AkUniqueID SHARED = 3574630834U;
        static const AkUniqueID TEST = 3157003241U;
        static const AkUniqueID VO = 1534528548U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID VO = 1534528548U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID GAME_DEFINED = 2820553813U;
        static const AkUniqueID GAME_DEFINED2 = 4237883901U;
        static const AkUniqueID USER_DEFINED = 2237892474U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
