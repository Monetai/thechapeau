// This class is Auto-Generated by the Script Tool in the Language Source
using UnityEngine;

namespace I2.Loc
{
	public static class ScriptLocalization
	{
		public static string Get(string Term) { return LocalizationManager.GetTermTranslation(Term, LocalizationManager.IsRight2Left, 0, false); }
		public static string Get(string Term, bool FixForRTL) { return LocalizationManager.GetTermTranslation(Term, FixForRTL, 0, false); }
		public static string Get(string Term, bool FixForRTL, int maxLineLengthForRTL) { return LocalizationManager.GetTermTranslation(Term, FixForRTL, maxLineLengthForRTL, false); }
		public static string Get(string Term, bool FixForRTL, int maxLineLengthForRTL, bool ignoreNumbers) { return LocalizationManager.GetTermTranslation(Term, FixForRTL, maxLineLengthForRTL, ignoreNumbers); }

        public static string GetOrTerm(string Term, string fallBack = null, bool FixForRTL = false)
        {
            if (fallBack == null)
                fallBack = Term;
            string ret = LocalizationManager.GetTermTranslation(Term, FixForRTL);
            return ret != "" ? ret : fallBack;
        }
    }
}