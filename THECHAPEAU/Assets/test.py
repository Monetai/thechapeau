import bpy
import sys
import os
import time

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()

argv = sys.argv
argv = argv[argv.index("--") + 1:]  # get all args after "--"

bpy.ops.import_scene.fbx(filepath=argv[0])
bpy.ops.wm.addon_enable(module="object_fracture_cell")

bpy.ops.object.select_all(action="SELECT")
for o in bpy.context.scene.objects:
	if o.type == 'MESH':
		bpy.context.scene.objects.active = o
		bpy.ops.object.mode_set(mode="EDIT")
		bpy.ops.mesh.subdivide()
		bpy.ops.object.mode_set(mode="OBJECT")

bpy.ops.object.add_fracture_cell_objects(source_limit=20,source=set(['VERT_OWN']),source_noise=1.,use_sharp_edges_apply=True,material_index=1,margin=0)

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()

# copy fractured objects from second layer to original layer
objects_on_2nd_layer = [ob for ob in bpy.context.scene.objects if ob.layers[1]]
for ob in objects_on_2nd_layer:
	ob.layers[0]=True;
	ob.layers[1]=False;

bpy.ops.object.select_all(action="SELECT")
bpy.ops.export_scene.fbx(filepath=os.path.splitext(argv[0])[0] + "_Fragmented.fbx");
bpy.ops.wm.quit_blender()