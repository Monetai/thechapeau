﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class WIFEvents : ScriptableObject
    {
        /*[HideInInspector]*/
        public string eventName;
        /*[HideInInspector]*/
        public string eventID;
    }
}